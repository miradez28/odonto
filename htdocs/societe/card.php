<?php
/* Copyright (C) 2001-2007  Rodolphe Quiedeville    <rodolphe@quiedeville.org>
 * Copyright (C) 2003       Brian Fraval            <brian@fraval.org>
 * Copyright (C) 2004-2015  Laurent Destailleur     <eldy@users.sourceforge.net>
 * Copyright (C) 2005       Eric Seigne             <eric.seigne@ryxeo.com>
 * Copyright (C) 2005-2017  Regis Houssin           <regis.houssin@inodbox.com>
 * Copyright (C) 2008       Patrick Raguin          <patrick.raguin@auguria.net>
 * Copyright (C) 2010-2016  Juanjo Menent           <jmenent@2byte.es>
 * Copyright (C) 2011-2013  Alexandre Spangaro      <aspangaro@open-dsi.fr>
 * Copyright (C) 2015       Jean-François Ferry     <jfefe@aternatik.fr>
 * Copyright (C) 2015       Marcos García           <marcosgdf@gmail.com>
 * Copyright (C) 2015       Raphaël Doursenaud      <rdoursenaud@gpcsolutions.fr>
 * Copyright (C) 2018       Nicolas ZABOURI	        <info@inovea-conseil.com>
 * Copyright (C) 2018       Ferran Marcet		    <fmarcet@2byte.es.com>
 * Copyright (C) 2018       Frédéric France         <frederic.france@netlogic.fr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *  \file       htdocs/societe/card.php
 *  \ingroup    societe
 *  \brief      Third party card page
 */

require '../main.inc.php';
require_once DOL_DOCUMENT_ROOT.'/core/lib/company.lib.php';
require_once DOL_DOCUMENT_ROOT.'/core/lib/images.lib.php';
require_once DOL_DOCUMENT_ROOT.'/core/lib/files.lib.php';
require_once DOL_DOCUMENT_ROOT.'/core/lib/functions.lib.php';
require_once DOL_DOCUMENT_ROOT.'/core/class/html.formadmin.class.php';
require_once DOL_DOCUMENT_ROOT.'/core/class/html.formcompany.class.php';
require_once DOL_DOCUMENT_ROOT.'/core/class/html.formfile.class.php';
require_once DOL_DOCUMENT_ROOT.'/core/class/extrafields.class.php';
require_once DOL_DOCUMENT_ROOT.'/contact/class/contact.class.php';
require_once DOL_DOCUMENT_ROOT.'/categories/class/categorie.class.php';
if (! empty($conf->adherent->enabled)) require_once DOL_DOCUMENT_ROOT.'/adherents/class/adherent.class.php';

$langs->loadLangs(array("companies","commercial","bills","banks","users"));
if (! empty($conf->categorie->enabled)) $langs->load("categories");
if (! empty($conf->incoterm->enabled)) $langs->load("incoterm");
if (! empty($conf->notification->enabled)) $langs->load("mails");

$mesg=''; $error=0; $errors=array();

$action		= (GETPOST('action', 'aZ09') ? GETPOST('action', 'aZ09') : 'view');
$cancel		= GETPOST('cancel', 'alpha');
$backtopage	= GETPOST('backtopage', 'alpha');
$confirm	= GETPOST('confirm', 'alpha');

$socid		= GETPOST('socid', 'int')?GETPOST('socid', 'int'):GETPOST('id', 'int');
if ($user->societe_id) $socid=$user->societe_id;
if (empty($socid) && $action == 'view') $action='create';

$object = new Societe($db);
$extrafields = new ExtraFields($db);

// fetch optionals attributes and labels
$extralabels=$extrafields->fetch_name_optionals_label($object->table_element);

// Initialize technical object to manage hooks of page. Note that conf->hooks_modules contains array of hook context
$hookmanager->initHooks(array('thirdpartycard','globalcard'));

if ($socid > 0) $object->fetch($socid);

if (! ($object->id > 0) && $action == 'view')
{
	$langs->load("errors");
	print($langs->trans('ErrorRecordNotFound'));
	exit;
}

// Get object canvas (By default, this is not defined, so standard usage of dolibarr)
$object->getCanvas($socid);
$canvas = $object->canvas?$object->canvas:GETPOST("canvas");
$objcanvas=null;
if (! empty($canvas))
{
    require_once DOL_DOCUMENT_ROOT.'/core/class/canvas.class.php';
    $objcanvas = new Canvas($db, $action);
    $objcanvas->getCanvas('thirdparty', 'card', $canvas);
}

// Security check
$result = restrictedArea($user, 'societe', $socid, '&societe', '', 'fk_soc', 'rowid', $objcanvas);


/*
 * Actions
 */

$parameters=array('id'=>$socid, 'objcanvas'=>$objcanvas);
$reshook=$hookmanager->executeHooks('doActions', $parameters, $object, $action);    // Note that $action and $object may have been modified by some hooks
if ($reshook < 0) setEventMessages($hookmanager->error, $hookmanager->errors, 'errors');

if (empty($reshook))
{
    if ($cancel)
    {
        $action='';
        if (! empty($backtopage))
        {
            header("Location: ".$backtopage);
            exit;
        }
    }

	if ($action == 'confirm_merge' && $confirm == 'yes' && $user->rights->societe->creer)
	{
		$error = 0;
		$soc_origin_id = GETPOST('soc_origin', 'int');
		$soc_origin = new Societe($db);

		if ($soc_origin_id <= 0)
		{
			$langs->load('errors');
			$langs->load('companies');
			setEventMessages($langs->trans('ErrorThirdPartyIdIsMandatory', $langs->transnoentitiesnoconv('MergeOriginThirdparty')), null, 'errors');
		}
		else
		{
			if (!$error && $soc_origin->fetch($soc_origin_id) < 1)
			{
				setEventMessages($langs->trans('ErrorRecordNotFound'), null, 'errors');
				$error++;
			}

			if (!$error)
			{
			    // TODO Move the merge function into class of object.

				$db->begin();

				// Recopy some data
				$object->client = $object->client | $soc_origin->client;
				$object->fournisseur = $object->fournisseur | $soc_origin->fournisseur;
				$listofproperties=array(
					'address', 'zip', 'town', 'state_id', 'country_id', 'phone', 'phone_pro', 'fax', 'email', 'skype', 'twitter', 'facebook', 'linkedin', 'url', 'barcode',
					'idprof1', 'idprof2', 'idprof3', 'idprof4', 'idprof5', 'idprof6',
					'tva_intra', 'effectif_id', 'forme_juridique', 'remise_percent', 'remise_supplier_percent', 'mode_reglement_supplier_id', 'cond_reglement_supplier_id', 'name_bis',
					'stcomm_id', 'outstanding_limit', 'price_level', 'parent', 'default_lang', 'ref', 'ref_ext', 'import_key', 'fk_incoterms', 'fk_multicurrency',
					'code_client', 'code_fournisseur', 'code_compta', 'code_compta_fournisseur',
					'model_pdf', 'fk_projet'
				);
				foreach ($listofproperties as $property)
				{
					if (empty($object->$property)) $object->$property = $soc_origin->$property;
				}

				// Concat some data
				$listofproperties=array(
				    'note_public', 'note_private'
				);
				foreach ($listofproperties as $property)
				{
				    $object->$property = dol_concatdesc($object->$property, $soc_origin->$property);
				}

				// Merge extrafields
				if (is_array($soc_origin->array_options))
				{
					foreach ($soc_origin->array_options as $key => $val)
					{
					    if (empty($object->array_options[$key])) $object->array_options[$key] = $val;
					}
				}

				// Merge categories
				$static_cat = new Categorie($db);

				$custcats_ori = $static_cat->containing($soc_origin->id, 'customer', 'id');
				$custcats = $static_cat->containing($object->id, 'customer', 'id');
				$custcats = array_merge($custcats, $custcats_ori);
				$object->setCategories($custcats, 'customer');

				$suppcats_ori = $static_cat->containing($soc_origin->id, 'supplier', 'id');
				$suppcats = $static_cat->containing($object->id, 'supplier', 'id');
				$suppcats = array_merge($suppcats, $suppcats_ori);
				$object->setCategories($suppcats, 'supplier');

				// If thirdparty has a new code that is same than origin, we clean origin code to avoid duplicate key from database unique keys.
				if ($soc_origin->code_client == $object->code_client
					|| $soc_origin->code_fournisseur == $object->code_fournisseur
					|| $soc_origin->barcode == $object->barcode)
				{
					dol_syslog("We clean customer and supplier code so we will be able to make the update of target");
					$soc_origin->code_client = '';
					$soc_origin->code_fournisseur = '';
					$soc_origin->barcode = '';
					$soc_origin->update($soc_origin->id, $user, 0, 1, 1, 'merge');
				}

				// Update
				$object->update($object->id, $user, 0, 1, 1, 'merge');
				if ($result < 0)
				{
					$error++;
				}

				// Move links
				if (! $error)
				{
					$objects = array(
						'Adherent' => '/adherents/class/adherent.class.php',
						'Societe' => '/societe/class/societe.class.php',
						//'Categorie' => '/categories/class/categorie.class.php',
						'ActionComm' => '/comm/action/class/actioncomm.class.php',
						'Propal' => '/comm/propal/class/propal.class.php',
						'Commande' => '/commande/class/commande.class.php',
						'Facture' => '/compta/facture/class/facture.class.php',
						'FactureRec' => '/compta/facture/class/facture-rec.class.php',
						'LignePrelevement' => '/compta/prelevement/class/ligneprelevement.class.php',
						'Contact' => '/contact/class/contact.class.php',
						'Contrat' => '/contrat/class/contrat.class.php',
						'Expedition' => '/expedition/class/expedition.class.php',
						'Fichinter' => '/fichinter/class/fichinter.class.php',
						'CommandeFournisseur' => '/fourn/class/fournisseur.commande.class.php',
						'FactureFournisseur' => '/fourn/class/fournisseur.facture.class.php',
						'SupplierProposal' => '/supplier_proposal/class/supplier_proposal.class.php',
						'ProductFournisseur' => '/fourn/class/fournisseur.product.class.php',
						'Livraison' => '/livraison/class/livraison.class.php',
						'Product' => '/product/class/product.class.php',
						'Project' => '/projet/class/project.class.php',
						'User' => '/user/class/user.class.php',
					);

					//First, all core objects must update their tables
					foreach ($objects as $object_name => $object_file)
					{
						require_once DOL_DOCUMENT_ROOT.$object_file;

						if (!$error && !$object_name::replaceThirdparty($db, $soc_origin->id, $object->id))
						{
							$error++;
							setEventMessages($db->lasterror(), null, 'errors');
						}
					}
				}

				// External modules should update their ones too
				if (! $error)
				{
    $reshook = $hookmanager->executeHooks('replaceThirdparty', array(
						'soc_origin' => $soc_origin->id,
						'soc_dest' => $object->id
					), $soc_dest, $action);

					if ($reshook < 0)
					{
						setEventMessages($hookmanager->error, $hookmanager->errors, 'errors');
						$error++;
					}
				}


				if (! $error)
				{
					$object->context=array('merge'=>1, 'mergefromid'=>$soc_origin->id);

					// Call trigger
					$result=$object->call_trigger('COMPANY_MODIFY', $user);
					if ($result < 0)
					{
						setEventMessages($object->error, $object->errors, 'errors');
						$error++;
					}
					// End call triggers
				}

				if (!$error)
				{
					//We finally remove the old thirdparty
					if ($soc_origin->delete($soc_origin->id, $user) < 1)
					{
						$error++;
					}
				}

				if (!$error)
				{
					setEventMessages($langs->trans('ThirdpartiesMergeSuccess'), null, 'mesgs');
					$db->commit();
				}
				else
				{
				    $langs->load("errors");
					setEventMessages($langs->trans('ErrorsThirdpartyMerge'), null, 'errors');
					$db->rollback();
				}
			}
		}
	}

    if (GETPOST('getcustomercode'))
    {
        // We defined value code_client
        $_POST["customer_code"]="Acompleter";
    }

    if (GETPOST('getsuppliercode'))
    {
        // We defined value code_fournisseur
        $_POST["supplier_code"]="Acompleter";
    }

    if($action=='set_localtax1')
    {
    	//obtidre selected del combobox
    	$value=GETPOST('lt1');
    	$object->fetch($socid);
    	$res=$object->setValueFrom('localtax1_value', $value, '', null, 'text', '', $user, 'COMPANY_MODIFY');
    }
    if($action=='set_localtax2')
    {
    	//obtidre selected del combobox
    	$value=GETPOST('lt2');
    	$object->fetch($socid);
    	$res=$object->setValueFrom('localtax2_value', $value, '', null, 'text', '', $user, 'COMPANY_MODIFY');
    }

    if ($action == 'update_extras') {
        $object->fetch($socid);

        $object->oldcopy = dol_clone($object);

        // Fill array 'array_options' with data from update form
        $extralabels = $extrafields->fetch_name_optionals_label($object->table_element);
        $ret = $extrafields->setOptionalsFromPost($extralabels, $object, GETPOST('attribute', 'none'));
        if ($ret < 0) $error++;

        if (! $error)
        {
        	$result = $object->insertExtraFields('COMPANY_MODIFY');
        	if ($result < 0)
        	{
        		setEventMessages($object->error, $object->errors, 'errors');
        		$error++;
        	}
        }

        if ($error) $action = 'edit_extras';
    }

    // Add new or update third party
    if ((! GETPOST('getcustomercode') && ! GETPOST('getsuppliercode'))
    && ($action == 'add' || $action == 'update') && $user->rights->societe->creer)
    {
        require_once DOL_DOCUMENT_ROOT.'/core/lib/functions2.lib.php';

        if (! GETPOST('name'))
        {
            setEventMessages($langs->trans("ErrorFieldRequired", $langs->transnoentitiesnoconv("Nombre del paciente")), null, 'errors');
            $error++;
        }
        if (GETPOST('client') < 0)
        {
            setEventMessages($langs->trans("ErrorFieldRequired", $langs->transnoentitiesnoconv("ProspectCustomer")), null, 'errors');
            $error++;
        }
        if (GETPOST('fournisseur') < 0)
        {
            setEventMessages($langs->trans("ErrorFieldRequired", $langs->transnoentitiesnoconv("Supplier")), null, 'errors');
            $error++;
        }

        if (! $error)
        {
        	if ($action == 'update')
	        {
	        	$ret=$object->fetch($socid);
				$object->oldcopy = clone $object;
	        }
			else $object->canvas=$canvas;

	        if (GETPOST("private", 'int') == 1)	// Ask to create a contact
	        {
	            $object->particulier		= GETPOST("private");

	            $object->name				= dolGetFirstLastname(GETPOST('firstname', 'alpha'), GETPOST('name', 'alpha'));
	            $object->civility_id		= GETPOST('civility_id');	// Note: civility id is a code, not an int
	            // Add non official properties
	            $object->name_bis			= GETPOST('name', 'alpha');
	            $object->firstname			= GETPOST('firstname', 'alpha');
	        }
	        else
	        {
	            $object->name				= GETPOST('name', 'alpha');
	        }
	        $object->entity					= (GETPOSTISSET('entity')?GETPOST('entity', 'int'):$conf->entity);
            $object->name_alias				= GETPOST('name_alias');
	        $object->address				= GETPOST('address');
	        $object->zip					= GETPOST('zipcode', 'alpha');
	        $object->town					= GETPOST('town', 'alpha');
	        $object->country_id				= GETPOST('country_id', 'int');
	        $object->state_id				= GETPOST('state_id', 'int');
	        $object->skype					= GETPOST('skype', 'alpha');
	        $object->twitter				= GETPOST('twitter', 'alpha');
	        $object->facebook				= GETPOST('facebook', 'alpha');
            $object->linkedin				= GETPOST('linkedin', 'alpha');
	        $object->phone					= GETPOST('phone', 'alpha');
	        $object->fax					= GETPOST('fax', 'alpha');
	        $object->email					= trim(GETPOST('email', 'custom', 0, FILTER_SANITIZE_EMAIL));
	        $object->url					= trim(GETPOST('url', 'custom', 0, FILTER_SANITIZE_URL));
	        $object->idprof1				= trim(GETPOST('idprof1', 'alpha'));
	        $object->idprof2				= trim(GETPOST('idprof2', 'alpha'));
	        $object->idprof3				= trim(GETPOST('idprof3', 'alpha'));
	        $object->idprof4				= trim(GETPOST('idprof4', 'alpha'));
	        $object->idprof5				= trim(GETPOST('idprof5', 'alpha'));
	        $object->idprof6				= trim(GETPOST('idprof6', 'alpha'));
	        $object->prefix_comm			= GETPOST('prefix_comm', 'alpha');
	        $object->code_client			= GETPOSTISSET('customer_code')?GETPOST('customer_code', 'alpha'):GETPOST('code_client', 'alpha');
	        $object->code_fournisseur		= GETPOSTISSET('supplier_code')?GETPOST('supplier_code', 'alpha'):GETPOST('code_fournisseur', 'alpha');
	        $object->capital				= GETPOST('capital', 'alpha');
	        $object->barcode				= GETPOST('barcode', 'alpha');

	        $object->tva_intra				= GETPOST('tva_intra', 'alpha');
	        $object->tva_assuj				= GETPOST('assujtva_value', 'alpha');
            $object->status					= GETPOST('status', 'alpha');
            
            //mis campos
            $object->tiposangre				= GETPOST('tiposangre', 'alpha');
            $object->genero					= GETPOST('genero', 'alpha');
            $object->nacimiento				= GETPOST('nacimiento', 'alpha');
            $object->civil					= GETPOST('civil', 'alpha');
            $object->edad					= GETPOST('edad', 'int');
            $object->ocupacion				= GETPOST('ocupacion', 'alpha');
            $object->contacto				= GETPOST('contacto', 'alpha');
            $object->parentesco				= GETPOST('parentesco', 'alpha');
            $object->observaciones			= GETPOST('observaciones', 'alpha');

	        // Local Taxes
	        $object->localtax1_assuj		= GETPOST('localtax1assuj_value', 'alpha');
	        $object->localtax2_assuj		= GETPOST('localtax2assuj_value', 'alpha');

	        $object->localtax1_value		= GETPOST('lt1', 'alpha');
	        $object->localtax2_value		= GETPOST('lt2', 'alpha');

	        $object->forme_juridique_code	= GETPOST('forme_juridique_code', 'int');
	        $object->effectif_id			= GETPOST('effectif_id', 'int');
	        $object->typent_id				= GETPOST('typent_id', 'int');

	        $object->typent_code			= dol_getIdFromCode($db, $object->typent_id, 'c_typent', 'id', 'code');	// Force typent_code too so check in verify() will be done on new type

	        $object->client					= GETPOST('client', 'int');
	        $object->fournisseur			= GETPOST('fournisseur', 'int');

	        $object->commercial_id			= GETPOST('commercial_id', 'int');
	        $object->default_lang			= GETPOST('default_lang');

	        // Webservices url/key
	        $object->webservices_url		= GETPOST('webservices_url', 'custom', 0, FILTER_SANITIZE_URL);
	        $object->webservices_key		= GETPOST('webservices_key', 'san_alpha');

			// Incoterms
			if (!empty($conf->incoterm->enabled))
			{
				$object->fk_incoterms		= GETPOST('incoterm_id', 'int');
				$object->location_incoterms	= GETPOST('location_incoterms', 'alpha');
			}

			// Multicurrency
			if (!empty($conf->multicurrency->enabled))
			{
				$object->multicurrency_code = GETPOST('multicurrency_code', 'alpha');
			}

	        // Fill array 'array_options' with data from add form
	        $ret = $extrafields->setOptionalsFromPost($extralabels, $object);
			if ($ret < 0)
			{
				 $error++;
			}

	        if (GETPOST('deletephoto')) $object->logo = '';
	        elseif (! empty($_FILES['photo']['name'])) $object->logo = dol_sanitizeFileName($_FILES['photo']['name']);

	        // Check parameters
	        if (! GETPOST('cancel', 'alpha'))
	        {
	            if (! empty($object->email) && ! isValidEMail($object->email))
	            {
	                $langs->load("errors");
	                $error++;
	                setEventMessages('', $langs->trans("ErrorBadEMail", $object->email), 'errors');
	            }
	            if (! empty($object->url) && ! isValidUrl($object->url))
	            {
	                $langs->load("errors");
	                setEventMessages('', $langs->trans("ErrorBadUrl", $object->url), 'errors');
	            }
	            if (! empty($object->webservices_url)) {
	                //Check if has transport, without any the soap client will give error
	                if (strpos($object->webservices_url, "http") === false)
	                {
	                    $object->webservices_url = "http://".$object->webservices_url;
	                }
	                if (! isValidUrl($object->webservices_url)) {
	                    $langs->load("errors");
	                    $error++; $errors[] = $langs->trans("ErrorBadUrl", $object->webservices_url);
	                }
	            }

	            // We set country_id, country_code and country for the selected country
	            $object->country_id=GETPOST('country_id')!=''?GETPOST('country_id'):$mysoc->country_id;
	            if ($object->country_id)
	            {
	            	$tmparray=getCountry($object->country_id, 'all');
	            	$object->country_code=$tmparray['code'];
	            	$object->country=$tmparray['label'];
	            }
	        }
        }

        if (! $error)
        {
            if ($action == 'add')
            {
            	$error = 0;

                $db->begin();

                if (empty($object->client))      $object->code_client='';
                if (empty($object->fournisseur)) $object->code_fournisseur='';

                $result = $object->create($user);

				if ($result >= 0)
				{
					if ($object->particulier)
					{
						dol_syslog("We ask to create a contact/address too", LOG_DEBUG);
						$result=$object->create_individual($user);
						if ($result < 0)
						{
							setEventMessages($object->error, $object->errors, 'errors');
							$error++;
						}
					}

					// Links with users
					$salesreps = GETPOST('commercial', 'array');
					$result = $object->setSalesRep($salesreps);
					if ($result < 0)
					{
						$error++;
						setEventMessages($object->error, $object->errors, 'errors');
					}

					// Customer categories association
					$custcats = GETPOST('custcats', 'array');
					$result = $object->setCategories($custcats, 'customer');
					if ($result < 0)
					{
						$error++;
						setEventMessages($object->error, $object->errors, 'errors');
					}

					// Supplier categories association
					$suppcats = GETPOST('suppcats', 'array');
					$result = $object->setCategories($suppcats, 'supplier');
					if ($result < 0)
					{
						$error++;
						setEventMessages($object->error, $object->errors, 'errors');
					}

                    // Logo/Photo save
                    $dir     = $conf->societe->multidir_output[$conf->entity]."/".$object->id."/logos/";
                    $file_OK = is_uploaded_file($_FILES['photo']['tmp_name']);
                    if ($file_OK)
                    {
                        if (image_format_supported($_FILES['photo']['name']))
                        {
                            dol_mkdir($dir);

                            if (@is_dir($dir))
                            {
                                $newfile=$dir.'/'.dol_sanitizeFileName($_FILES['photo']['name']);
                                $result = dol_move_uploaded_file($_FILES['photo']['tmp_name'], $newfile, 1);

                                if (! $result > 0)
                                {
                                    $errors[] = "ErrorFailedToSaveFile";
                                }
                                else
                                {
                                    // Create thumbs
                                    $object->addThumbs($newfile);
                                }
                            }
                        }
                    }
                    else
					{
						switch($_FILES['photo']['error'])
						{
						    case 1: //uploaded file exceeds the upload_max_filesize directive in php.ini
						    case 2: //uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the html form
						      $errors[] = "ErrorFileSizeTooLarge";
						      break;
	      					case 3: //uploaded file was only partially uploaded
						      $errors[] = "ErrorFilePartiallyUploaded";
						      break;
						}
	                }
                    // Gestion du logo de la société
                }
                else
				{
				    if ($db->lasterrno() == 'DB_ERROR_RECORD_ALREADY_EXISTS') // TODO Sometime errors on duplicate on profid and not on code, so we must manage this case
					{
						$duplicate_code_error = true;
						$object->code_fournisseur = null;
						$object->code_client = null;
					}

                    setEventMessages($object->error, $object->errors, 'errors');
                   	$error++;
                }

                if ($result >= 0 && ! $error)
                {
                    $db->commit();

                	if (! empty($backtopage))
                	{
                	    if (preg_match('/\?/', $backtopage)) $backtopage.='&socid='.$object->id;
               		    header("Location: ".$backtopage);
                    	exit;
                	}
                	else
                	{
                    	$url=$_SERVER["PHP_SELF"]."?socid=".$object->id;
                    	if (($object->client == 1 || $object->client == 3) && empty($conf->global->SOCIETE_DISABLE_CUSTOMERS)) $url=DOL_URL_ROOT."/comm/card.php?socid=".$object->id;
                    	elseif ($object->fournisseur == 1) $url=DOL_URL_ROOT."/fourn/card.php?socid=".$object->id;

                		header("Location: ".$url);
                    	exit;
                	}
                }
                else
                {
                    $db->rollback();
                    $action='create';
                }
            }

            if ($action == 'update')
            {
            	$error = 0;

                if (GETPOST('cancel', 'alpha'))
                {
                	if (! empty($backtopage))
                	{
               		    header("Location: ".$backtopage);
                    	exit;
                	}
                	else
                	{
               		    header("Location: ".$_SERVER["PHP_SELF"]."?socid=".$socid);
                    	exit;
                	}
                }

                // To not set code if third party is not concerned. But if it had values, we keep them.
                if (empty($object->client) && empty($object->oldcopy->code_client))          $object->code_client='';
                if (empty($object->fournisseur)&& empty($object->oldcopy->code_fournisseur)) $object->code_fournisseur='';
                //var_dump($object);exit;

                $result = $object->update($socid, $user, 1, $object->oldcopy->codeclient_modifiable(), $object->oldcopy->codefournisseur_modifiable(), 'update', 0);
                if ($result <=  0)
                {
                    setEventMessages($object->error, $object->errors, 'errors');
                    $error++;
                }

				// Links with users
				$salesreps = GETPOST('commercial', 'array');
				$result = $object->setSalesRep($salesreps);
				if ($result < 0)
				{
					$error++;
					setEventMessages($object->error, $object->errors, 'errors');
				}

				// Prevent thirdparty's emptying if a user hasn't rights $user->rights->categorie->lire (in such a case, post of 'custcats' is not defined)
				if (! $error && !empty($user->rights->categorie->lire))
				{
					// Customer categories association
					$categories = GETPOST('custcats', 'array');
					$result = $object->setCategories($categories, 'customer');
					if ($result < 0)
					{
						$error++;
						setEventMessages($object->error, $object->errors, 'errors');
					}

					// Supplier categories association
					$categories = GETPOST('suppcats', 'array');
					$result = $object->setCategories($categories, 'supplier');
					if ($result < 0)
					{
						$error++;
						setEventMessages($object->error, $object->errors, 'errors');
					}
				}

                // Logo/Photo save
                $dir     = $conf->societe->multidir_output[$object->entity]."/".$object->id."/logos";
                $file_OK = is_uploaded_file($_FILES['photo']['tmp_name']);
                if (GETPOST('deletephoto') && $object->logo)
                {
                    $fileimg=$dir.'/'.$object->logo;
                    $dirthumbs=$dir.'/thumbs';
                    dol_delete_file($fileimg);
                    dol_delete_dir_recursive($dirthumbs);
                }
                if ($file_OK)
                {
                    if (image_format_supported($_FILES['photo']['name']) > 0)
                    {
                        dol_mkdir($dir);

                        if (@is_dir($dir))
                        {
                            $newfile=$dir.'/'.dol_sanitizeFileName($_FILES['photo']['name']);
                            $result = dol_move_uploaded_file($_FILES['photo']['tmp_name'], $newfile, 1);

                            if (! $result > 0)
                            {
                                $errors[] = "ErrorFailedToSaveFile";
                            }
                            else
                            {
                            	// Create thumbs
                            	$object->addThumbs($newfile);

                                // Index file in database
                                if (! empty($conf->global->THIRDPARTY_LOGO_ALLOW_EXTERNAL_DOWNLOAD))
                                {
                                	require_once DOL_DOCUMENT_ROOT .'/core/lib/files.lib.php';
                                	// the dir dirname($newfile) is directory of logo, so we should have only one file at once into index, so we delete indexes for the dir
                                	deleteFilesIntoDatabaseIndex(dirname($newfile), '', '');
                                	// now we index the uploaded logo file
                                	addFileIntoDatabaseIndex(dirname($newfile), basename($newfile), '', 'uploaded', 1);
                                }
                            }
                        }
                    }
                    else
					{
                        $errors[] = "ErrorBadImageFormat";
                    }
                }
                else
                {
					switch($_FILES['photo']['error'])
					{
					    case 1: //uploaded file exceeds the upload_max_filesize directive in php.ini
					    case 2: //uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the html form
					      $errors[] = "ErrorFileSizeTooLarge";
					      break;
      					case 3: //uploaded file was only partially uploaded
					      $errors[] = "ErrorFilePartiallyUploaded";
					      break;
					}
                }
                // Gestion du logo de la société


                // Update linked member
                if (! $error && $object->fk_soc > 0)
                {

                	$sql = "UPDATE ".MAIN_DB_PREFIX."adherent";
                	$sql.= " SET fk_soc = NULL WHERE fk_soc = " . $id;
                	if (! $object->db->query($sql))
                	{
                		$error++;
                		$object->error .= $object->db->lasterror();
                		setEventMessages($object->error, $object->errors, 'errors');
                	}
                }

                if (! $error && ! count($errors))
                {
                	if (! empty($backtopage))
                	{
               		    header("Location: ".$backtopage);
                    	exit;
                	}
                	else
                	{
               		    header("Location: ".$_SERVER["PHP_SELF"]."?socid=".$socid);
                    	exit;
                	}
                }
                else
                {
                    $object->id = $socid;
                    $action= "edit";
                }
            }
        }
        else
        {
        	$action = ($action=='add'?'create':'edit');
        }
    }

    // Delete third party
    if ($action == 'confirm_delete' && $confirm == 'yes' && $user->rights->societe->supprimer)
    {
        $object->fetch($socid);
        $result = $object->delete($socid, $user);

        if ($result > 0)
        {
            header("Location: ".DOL_URL_ROOT."/societe/list.php?restore_lastsearch_values=1&delsoc=".urlencode($object->name));
            exit;
        }
        else
        {
            $langs->load("errors");
           	setEventMessages($object->error, $object->errors, 'errors');
           	$error++;
            $action='';
        }
    }

    // Set parent company
    if ($action == 'set_thirdparty' && $user->rights->societe->creer)
    {
    	$object->fetch($socid);
    	$result = $object->set_parent(GETPOST('editparentcompany', 'int'));
    }

    // Set incoterm
    if ($action == 'set_incoterms' && !empty($conf->incoterm->enabled))
    {
    	$object->fetch($socid);
    	$result = $object->setIncoterms(GETPOST('incoterm_id', 'int'), GETPOST('location_incoterms', 'alpha'));
    }

    $id=$socid;
    $object->fetch($socid);

    // Actions to send emails
    $trigger_name='COMPANY_SENTBYMAIL';
    $paramname='socid';
    $mode='emailfromthirdparty';
    $trackid='thi'.$object->id;
    include DOL_DOCUMENT_ROOT.'/core/actions_sendmails.inc.php';

    // Actions to build doc
    $id = $socid;
    $upload_dir = $conf->societe->dir_output;
    $permissioncreate=$user->rights->societe->creer;
    include DOL_DOCUMENT_ROOT.'/core/actions_builddoc.inc.php';
}


/*
 *  View
 */

$form = new Form($db);
$formfile = new FormFile($db);
$formadmin = new FormAdmin($db);
$formcompany = new FormCompany($db);

if ($socid > 0 && empty($object->id))
{
    $result=$object->fetch($socid);
	if ($result <= 0) dol_print_error('', $object->error);
}

$title=$langs->trans("ThirdParty");
if (! empty($conf->global->MAIN_HTML_TITLE) && preg_match('/thirdpartynameonly/', $conf->global->MAIN_HTML_TITLE) && $object->name) $title=$object->name." - ".$langs->trans('Card');
$help_url='EN:Module_Third_Parties|FR:Module_Tiers|ES:Empresas';
llxHeader('', $title, $help_url);

$countrynotdefined=$langs->trans("ErrorSetACountryFirst").' ('.$langs->trans("SeeAbove").')';

if (is_object($objcanvas) && $objcanvas->displayCanvasExists($action))
{
    // -----------------------------------------
    // When used with CANVAS
    // -----------------------------------------
   	$objcanvas->assign_values($action, $object->id, $object->ref);	// Set value for templates
    $objcanvas->display_canvas($action);							// Show template
}
else
{
    // -----------------------------------------
    // When used in standard mode
    // -----------------------------------------
    if ($action == 'create')
    {
        /*
         *  Creation
         */
		$private=GETPOST("private", "int");
		if (! empty($conf->global->THIRDPARTY_DEFAULT_CREATE_CONTACT) && ! isset($_GET['private']) && ! isset($_POST['private'])) $private=1;
    	if (empty($private)) $private=0;

        // Load object modCodeTiers
        $module=(! empty($conf->global->SOCIETE_CODECLIENT_ADDON)?$conf->global->SOCIETE_CODECLIENT_ADDON:'mod_codeclient_leopard');
        if (substr($module, 0, 15) == 'mod_codeclient_' && substr($module, -3) == 'php')
        {
            $module = substr($module, 0, dol_strlen($module)-4);
        }
        $dirsociete=array_merge(array('/core/modules/societe/'), $conf->modules_parts['societe']);
        foreach ($dirsociete as $dirroot)
        {
            $res=dol_include_once($dirroot.$module.'.php');
            if ($res) break;
        }
        $modCodeClient = new $module;
        // Load object modCodeFournisseur
        $module=(! empty($conf->global->SOCIETE_CODECLIENT_ADDON)?$conf->global->SOCIETE_CODECLIENT_ADDON:'mod_codeclient_leopard');
        if (substr($module, 0, 15) == 'mod_codeclient_' && substr($module, -3) == 'php')
        {
            $module = substr($module, 0, dol_strlen($module)-4);
        }
        $dirsociete=array_merge(array('/core/modules/societe/'), $conf->modules_parts['societe']);
        foreach ($dirsociete as $dirroot)
        {
            $res=dol_include_once($dirroot.$module.'.php');
            if ($res) break;
        }
        $modCodeFournisseur = new $module;

        // Define if customer/prospect or supplier status is set or not
        if (GETPOST("type")!='f')
        {
            $object->client=-1;
            if (! empty($conf->global->THIRDPARTY_CUSTOMERPROSPECT_BY_DEFAULT))  { $object->client=3; }
        }
        // Prospect / Customer
        if (GETPOST("type")=='c')  {
        	if (! empty($conf->global->THIRDPARTY_CUSTOMERTYPE_BY_DEFAULT)) {
        		$object->client=$conf->global->THIRDPARTY_CUSTOMERTYPE_BY_DEFAULT;
        	} else {
        		$object->client=3;
        	}
        }
        if (GETPOST("type")=='p')  { $object->client=2; }
        if (! empty($conf->fournisseur->enabled) && (GETPOST("type")=='f' || (GETPOST("type")=='' && ! empty($conf->global->THIRDPARTY_SUPPLIER_BY_DEFAULT))))  { $object->fournisseur=1; }

        $object->name				= GETPOST('name', 'alpha');
        $object->firstname			= GETPOST('firstname', 'alpha');
        $object->particulier		= $private;
        $object->prefix_comm		= GETPOST('prefix_comm', 'alpha');
        $object->client				= GETPOST('client', 'int')?GETPOST('client', 'int'):$object->client;

        if (empty($duplicate_code_error)) {
	        $object->code_client		= GETPOST('customer_code', 'alpha');
	        $object->fournisseur		= GETPOST('fournisseur')?GETPOST('fournisseur'):$object->fournisseur;
            $object->code_fournisseur	= GETPOST('supplier_code', 'alpha');
        }
		else {
			setEventMessages($langs->trans('NewCustomerSupplierCodeProposed'), '', 'warnings');
		}


        $object->address			= GETPOST('address', 'alpha');
        $object->zip				= GETPOST('zipcode', 'alpha');
        $object->town				= GETPOST('town', 'alpha');
        $object->state_id			= GETPOST('state_id', 'int');
        $object->skype				= GETPOST('skype', 'alpha');
        $object->twitter			= GETPOST('twitter', 'alpha');
        $object->facebook			= GETPOST('facebook', 'alpha');
        $object->linkedin			= GETPOST('linkedin', 'alpha');
        $object->phone				= GETPOST('phone', 'alpha');
        $object->fax				= GETPOST('fax', 'alpha');
        $object->email				= GETPOST('email', 'custom', 0, FILTER_SANITIZE_EMAIL);
        $object->url				= GETPOST('url', 'custom', 0, FILTER_SANITIZE_URL);
        $object->capital			= GETPOST('capital', 'alpha');
        $object->barcode			= GETPOST('barcode', 'alpha');
        $object->idprof1			= GETPOST('idprof1', 'alpha');
        $object->idprof2			= GETPOST('idprof2', 'alpha');
        $object->idprof3			= GETPOST('idprof3', 'alpha');
        $object->idprof4			= GETPOST('idprof4', 'alpha');
        $object->idprof5			= GETPOST('idprof5', 'alpha');
        $object->idprof6			= GETPOST('idprof6', 'alpha');
        $object->typent_id			= GETPOST('typent_id', 'int');
        $object->effectif_id		= GETPOST('effectif_id', 'int');
        $object->civility_id		= GETPOST('civility_id', 'alpha');

        $object->tva_assuj			= GETPOST('assujtva_value', 'int');
        $object->status				= GETPOST('status', 'int');

        //mis campos
        $object->tiposangre				= GETPOST('phone', 'alpha');
        $object->genero					= GETPOST('genero', 'alpha');
        $object->nacimiento				= GETPOST('nacimiento', 'alpha');
        $object->civil					= GETPOST('civil', 'alpha');
        $object->edad					= GETPOST('edad', 'int');
        $object->ocupacion				= GETPOST('ocupacion', 'alpha');
        $object->contacto				= GETPOST('contacto', 'alpha');
        $object->parentesco				= GETPOST('parentesco', 'alpha');
        $object->observaciones			= GETPOST('observaciones', 'alpha');


        //Local Taxes
        $object->localtax1_assuj	= GETPOST('localtax1assuj_value', 'int');
        $object->localtax2_assuj	= GETPOST('localtax2assuj_value', 'int');

        $object->localtax1_value	=GETPOST('lt1', 'int');
        $object->localtax2_value	=GETPOST('lt2', 'int');

        $object->tva_intra			= GETPOST('tva_intra', 'alpha');

        $object->commercial_id		= GETPOST('commercial_id', 'int');
        $object->default_lang		= GETPOST('default_lang');

        $object->logo = (isset($_FILES['photo'])?dol_sanitizeFileName($_FILES['photo']['name']):'');

        // Gestion du logo de la société
        $dir     = $conf->societe->multidir_output[$conf->entity]."/".$object->id."/logos";
        $file_OK = (isset($_FILES['photo'])?is_uploaded_file($_FILES['photo']['tmp_name']):false);
        if ($file_OK)
        {
            if (image_format_supported($_FILES['photo']['name']))
            {
                dol_mkdir($dir);

                if (@is_dir($dir))
                {
                    $newfile=$dir.'/'.dol_sanitizeFileName($_FILES['photo']['name']);
                    $result = dol_move_uploaded_file($_FILES['photo']['tmp_name'], $newfile, 1);

                    if (! $result > 0)
                    {
                        $errors[] = "ErrorFailedToSaveFile";
                    }
                    else
                    {
                        // Create thumbs
                        $object->addThumbs($newfile);
                    }
                }
            }
        }

        // We set country_id, country_code and country for the selected country
        $object->country_id=GETPOST('country_id')?GETPOST('country_id'):$mysoc->country_id;
        if ($object->country_id)
        {
            $tmparray=getCountry($object->country_id, 'all');
            $object->country_code=$tmparray['code'];
            $object->country=$tmparray['label'];
        }
        $object->forme_juridique_code=GETPOST('forme_juridique_code');
        /* Show create form */

        //$linkback="";
        //print load_fiche_titre($langs->trans("NewThirdParty"), $linkback, 'title_companies.png');

        if (! empty($conf->use_javascript_ajax) && ! empty($conf->global->THIRDPARTY_SUGGEST_ALSO_ADDRESS_CREATION))
        {
            print "\n".'<script type="text/javascript">';
            print '$(document).ready(function () {
						id_te_private=8;
                        id_ef15=1;
                        is_private='.$private.';
						if (is_private) {
							$(".individualline").show();
						} else {
							$(".individualline").hide();
						}
                        $("#radiocompany").click(function() {
                        	$(".individualline").hide();
                        	$("#typent_id").val(0);
                        	$("#effectif_id").val(0);
                        	$("#TypeName").html(document.formsoc.Nombre del paciente.value);
                        	document.formsoc.private.value=0;
                        });
                        $("#radioprivate").click(function() {
                        	$(".individualline").show();
                        	$("#typent_id").val(id_te_private);
                        	$("#effectif_id").val(id_ef15);
                        	$("#TypeName").html(document.formsoc.LastName.value);
                        	document.formsoc.private.value=1;
                        });

						init_customer_categ();
			  			$("#customerprospect").change(function() {
								init_customer_categ();
						});
						function init_customer_categ() {
								console.log("is customer or prospect = "+jQuery("#customerprospect").val());
								if (jQuery("#customerprospect").val() == 0 && (jQuery("#fournisseur").val() == 0 || '.(empty($conf->global->THIRDPARTY_CAN_HAVE_CATEGORY_EVEN_IF_NOT_CUSTOMER_PROSPECT_SUPPLIER)?'1':'0').'))
								{
									jQuery(".visibleifcustomer").hide();
								}
								else
								{
									jQuery(".visibleifcustomer").show();
								}
						}

						init_supplier_categ();
			       		$("#fournisseur").change(function() {
							init_supplier_categ();
						});
						function init_supplier_categ() {
								console.log("is supplier = "+jQuery("#fournisseur").val());
								if (jQuery("#fournisseur").val() == 0)
								{
									jQuery(".visibleifsupplier").hide();
								}
								else
								{
									jQuery(".visibleifsupplier").show();
								}
						}

                        $("#selectcountry_id").change(function() {
                        	document.formsoc.action.value="create";
                        	document.formsoc.submit();
                        });
                     });';
            print '</script>'."\n";

            print '<div id="selectthirdpartytype">';
            print '<div class="hideonsmartphone float">';
            print $langs->trans("ThirdPartyType").': &nbsp; &nbsp; ';
            print '</div>';
	        print '<label for="radiocompany" class="radiocompany">';
            print '<input type="radio" id="radiocompany" class="flat" name="private"  value="0"'.($private?'':' checked').'>';
	        print '&nbsp;';
            print $langs->trans("CreateThirdPartyOnly");
	        print '</label>';
            print ' &nbsp; &nbsp; ';
	        print '<label for="radioprivate" class="radioprivate">';
            $text ='<input type="radio" id="radioprivate" class="flat" name="private" value="1"'.($private?' checked':'').'>';
	        $text.='&nbsp;';
	        $text.= $langs->trans("CreateThirdPartyAndContact");
	        $htmltext=$langs->trans("ToCreateContactWithSameName");
	        print $form->textwithpicto($text, $htmltext, 1, 'help', '', 0, 3);
            print '</label>';
            print '</div>';
            print "<br>\n";
        }

        dol_htmloutput_mesg(is_numeric($error)?'':$error, $errors, 'error');

        print '<form enctype="multipart/form-data" action="'.$_SERVER["PHP_SELF"].'" method="post" name="formsoc" autocomplete="off">';		// Chrome ignor autocomplete

        print '<input type="hidden" name="action" value="add">';
        print '<input type="hidden" name="backtopage" value="'.$backtopage.'">';
        print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
        print '<input type="hidden" name="private" value='.$object->particulier.'>';
        print '<input type="hidden" name="type" value='.GETPOST("type", 'alpha').'>';
        print '<input type="hidden" name="LastName" value="'.$langs->trans('Nombre del paciente').' / '.$langs->trans('LastName').'">';
        print '<input type="hidden" name="Nombre del paciente" value="'.$langs->trans('Nombre del paciente').'">';
        if ($modCodeClient->code_auto || $modCodeFournisseur->code_auto) print '<input type="hidden" name="code_auto" value="1">';

        //dol_fiche_head(null, 'card', '', 0, '');

        print '<div class="center">';
        print '<label class="fieldrequired">AGREGAR PACIENTE</label>';
        print '</div>'."\n";
        print '<br>';

        print '<label class="fieldrequired">'.$form->editfieldkey('ID Paciente', 'customer_code', '', $object, 0).'</label>';
		$tmpcode=$object->code_client;
        if (empty($tmpcode) && ! empty($modCodeClient->code_auto)) $tmpcode=$modCodeClient->getNextValue($object, 0);
        print '<input type="text" name="customer_code" id="customer_code" class="maxwidthonsmartphone" value="'.dol_escape_htmltag($tmpcode).'" maxlength="15">';
        $s=$modCodeClient->getToolTip($langs, $object, 0);
        print $form->textwithpicto('', $s, 1);


        
        // Ajout du logo
        print '<table class="border" width="100%">';

        print '<tr>';
        print '<td rowspan="2">';
        print ' <label for="photoinput" style="float: right;">
    			<img style="width:150px;height:150px;border-radius:150px;background:#9d9d9c;"  id="imagen"/>
  				</label>

  				<input style="display: none;" name="photo" id="photoinput"  type="file" />';

        print '</td>';

        //nombre

        print '<td >';
        
		print '<span id="TypeName" class="fieldrequired">'.$form->editfieldkey('Nombre del paciente', 'name', '', $object, 0).'</span>';
        
        print '<input type="text"  maxlength="128" name="name" id="name" value="'.$object->name.'" autofocus="autofocus">';
        
        print '</td >';

        //apellido

        print '<tr>';

        print '<td >';
        
		print '<span id="TypeName" class="fieldrequired">'.$form->editfieldkey('Apellido del paciente', 'name', '', $object, 0).'</span>';
        
        print '<input type="text"  maxlength="128" name="name" id="name" value="'.$object->name.'" autofocus="autofocus">';
        
        print '</td >';
        
        print '</tr>';
        
        //tipo de paciente

        print '<tr>';
        
        print '<td >';

        print '<span id="TypeName" class="fieldrequired"  style="float: right;">'.$form->editfieldkey('Paciente potencial / Paciente', 'customerprospect', '', $object, 0, 'string', '', 1).'</span>';
        
        print '</td >';

        print '<td >';

        $selected=(GETPOSTISSET('client', 'int')?GETPOST('client', 'int'):$object->client);
	    print $formcompany->selectProspectCustomerType($selected);

        print '</td >';
        
        print '</tr>';

        //estado

        print '<tr>';
        
        print '<td>';
        
        print '<span id="TypeName" class="fieldrequired"  style="float: right;">'.$form->editfieldkey('Status', 'status', '', $object, 0).'</span>';

        print '</td >';

        print '<td >';

        print $form->selectarray('status', array('0'=>$langs->trans('Activo'),'1'=>$langs->trans('Activo')), 1);

        print '</td>';

        print '</tr>';

        //direccion

        print '<tr>';
        
        print '<td>';
        
        print '<span id="TypeName" class="fieldrequired"  style="float: right;">'.$form->editfieldkey('Address', 'address', '', $object, 0).'</span>';

        print '</td >';

        print '<td >';

        print '<textarea name="address" id="address"  rows="'.ROWS_2.'" wrap="soft">';
        print $object->address;
        print '</textarea>';

        print '</td>';

        print '</tr>';

        //correo

        print '<tr>';
        
        print '<td>';
        
        print '<span id="TypeName" class="fieldrequired"  style="float: right;">'.$form->editfieldkey('EMail', 'email', '', $object, 0, 'string', '', $conf->global->SOCIETE_EMAIL_MANDATORY).'</span>';

        print '</td >';

        print '<td >';

        print '<input type="text" name="email" id="email" value="'.$object->email.'">';

        print '</td>';

        print '</tr>';

        //telefono

        print '<tr>';
        
        print '<td>';
        
        print '<span id="TypeName" class="fieldrequired" style="float: right;">'.$form->editfieldkey('Phone', 'phone', '', $object, 0).'</span>';

        print '</td >';

        print '<td >';

        print '<input type="text" name="phone" id="phone" value="'.$object->phone.'">';

        print '</td>';

        print '</tr>';

        //genero

        print '<tr>';
        
        print '<td>';
        
        print '<span id="TypeName" class="fieldrequired" style="float: right;">'.$form->editfieldkey('Genero', 'genero', '', $object, 0).'</span>';

        print '</td >';

        print '<td >';

        print $form->selectarray('genero', array('Masculino'=>$langs->trans('Masculino'),'Femenino'=>$langs->trans('Femenino')), 1);

        print '</td>';

        print '</tr>';

        //fecha de nacimiento

        print '<tr>';


        
        print '<td>';
        
        print '<span id="TypeName" class="fieldrequired" style="float: right;">'.$form->editfieldkey('Fecha de nacimiento', 'nacimiento', '', $object, 0).'</span>';

        print '</td >';

        print '<td >';

        print '<input type="date" name="nacimiento" id="nacimiento"  value="'.$object->phone.'">';

        print '</td>';

        //edad

        print '<td>';
        
        print '<span id="TypeName" class="fieldrequired">'.$form->editfieldkey('Edad', 'edad', '', $object, 0).'</span>';

        print '</td >';

        print '<td >';

        print '<input type="text" name="edad" id="edad" value="'.$object->edad.'" style="background:transparent">';

        print '</td>';



        print '</tr>';


        //grupo sanguinio

        print '<tr>';
        
        print '<td>';
        
        print '<span id="TypeName" class="fieldrequired" style="float: right;">'.$form->editfieldkey('Grupo sanguineo', 'tiposangre', '', $object, 0).'</span>';

        print '</td >';

        print '<td >';

        print '<input type="text" name="tiposangre" id="tiposangre" value="'.$object->tiposangre.'">';

        print '</td>';

        print '</tr>';

        //estado civil

        print '<tr>';
        
        print '<td>';
        
        print '<span id="TypeName" class="fieldrequired" style="float: right;">'.$form->editfieldkey('Estado civil', 'civil', '', $object, 0).'</span>';

        print '</td >';

        print '<td >';

        print '<input type="text" name="civil" id="civil" value="'.$object->civil.'">';

        print '</td>';

        print '</tr>';

        //ocupacion

        print '<tr>';
        
        print '<td>';
        
        print '<span id="TypeName" class="fieldrequired" style="float: right;">'.$form->editfieldkey('Ocupacion', 'ocupacion', '', $object, 0).'</span>';

        print '</td >';

        print '<td >';

        print '<input type="text" name="ocupacion" id="ocupacion" value="'.$object->ocupacion.'">';

        print '</td>';

        print '</tr>';

        //familiares 

        print '<tr>';
        
        print '<td>';
        
        print '<span id="TypeName" class="fieldrequired">'. '</td><td>' . $langs->trans("Seleccione al familiar") .'</span>';

        print '</td >';

        print '</tr>';

        //opcionfamilair

        print '<tr>';
        
        print '<td>';
        
        print '<span id="TypeName" class="fieldrequired" style="float: right;">' . $langs->trans('Familiar registrado') . '</span>';

        print '</td >';

        print '<td >';

        print $form->select_company($soc->id, 'contacto', '((s.client = 1 OR s.client = 3) AND s.status=1)', 'SelectThirdParty', 0, 0, null, 0, 'minwidth300');
		// Option to reload page to retrieve customer informations. Note, this clear other input
		if (!empty($conf->global->RELOAD_PAGE_ON_CUSTOMER_CHANGE))
		{
			print '<script type="text/javascript">
			$(document).ready(function() {
				$("#contacto").change(function() {
					var contacto = $(this).val();
			        var fac_rec = $(\'#fac_rec\').val();
					// reload page
        			window.location.href = "'.$_SERVER["PHP_SELF"].'?action=create&contacto="+contacto+"&fac_rec="+fac_rec;
				});
			});
			</script>';
		};

        print '</td>';

        //parentezco

        print '<td>';
        
        print '<span id="TypeName" class="fieldrequired">'.$form->editfieldkey('Parentesco', 'parentesco', '', $object, 0).'</span>';

        print '</td >';

        print '<td >';

        print '<input type="text" name="parentesco" id="parentesco" value="'.$object->parentesco.'">';

        print '</td>';

        print '</tr>';

        //observaciones

        print '<tr>';
        
        print '<td>';
        
        print '<span id="TypeName" class="fieldrequired" style="float: right;">'.$form->editfieldkey('Observación / Nota', 'observaciones', '', $object, 0).'</span>';

        print '</td >';

        print '<td >';

        print '<textarea name="observaciones" id="observaciones" rows="'.ROWS_2.'" wrap="soft">';

        print $object->observaciones;

        print '</textarea>';

        print '</td>';

        print '</tr>';


        print '</tr>';

        print '</table>';   


        print '<script type="text/javascript">
			function init() {
			  var inputFile = document.getElementById("photoinput");
			  photoinput.addEventListener("change", mostrarImagen, false);
			}			

			function mostrarImagen(event) {
			  var file = event.target.files[0];
			  var reader = new FileReader();
			  reader.onload = function(event) {
			    var img = document.getElementById("imagen");
			    img.src= event.target.result;
			  }
			  reader.readAsDataURL(file);
			}			

			window.addEventListener("load", init, false);
            </script>';
            
            print '<script type="text/javascript">
			$(function(){
            $("#nacimiento").on("change", calcularEdad);
        });
        
        function calcularEdad() {
            
            fecha = $(this).val();
            var hoy = new Date();
            var cumpleanos = new Date(fecha);
            var edad = hoy.getFullYear() - cumpleanos.getFullYear();
            var m = hoy.getMonth() - cumpleanos.getMonth();

            if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
                edad--;
            }
            $("#edad").val(edad);
        }
            </script>';
        
        // // If javascript on, we show option individual
        // if ($conf->use_javascript_ajax)
        // {
        // 	if (! empty($conf->global->THIRDPARTY_SUGGEST_ALSO_ADDRESS_CREATION))
        // 	{
        // 		// Firstname
	    //         print '<tr class="individualline"><td>'.$form->editfieldkey('FirstName', 'firstname', '', $object, 0).'</td>';
		//         print '<td colspan="3"><input type="text" class="minwidth300" maxlength="128" name="firstname" id="firstname" value="'.$object->firstname.'"></td>';
	    //         print '</tr>';

	    //         // Title
	    //         print '<tr class="individualline"><td>'.$form->editfieldkey('UserTitle', 'civility_id', '', $object, 0).'</td><td colspan="3" class="maxwidthonsmartphone">';
	    //         print $formcompany->select_civility($object->civility_id, 'civility_id', 'maxwidth100').'</td>';
	    //         print '</tr>';
        // 	}
        // }

        // Alias names (commercial, trademark or alias names)
        //print '<tr id="name_alias"><td><label for="name_alias_input">'.$langs->trans('AliasNames').'</label></td>';
	    //print '<td colspan="3"><input type="text" class="minwidth300" name="name_alias" id="name_alias_input" value="'.$object->name_alias.'"></td></tr>';


        
        // if ((! empty($conf->fournisseur->enabled) && ! empty($user->rights->fournisseur->lire))
        // 	|| (! empty($conf->supplier_proposal->enabled) && ! empty($user->rights->supplier_proposal->lire)))
        // {
        //     // Supplier
        //     // print '<tr>';
        //     // print '<td>'.$form->editfieldkey('Vendor', 'fournisseur', '', $object, 0, 'string', '', 1).'</td><td>';
        //     // $default = -1;
        //     // if (! empty($conf->global->THIRDPARTY_SUPPLIER_BY_DEFAULT)) $default=1;
        //     // print $form->selectyesno("fournisseur", (GETPOST('fournisseur', 'int')!=''?GETPOST('fournisseur', 'int'):(GETPOST("type", 'alpha') == '' ? $default : $object->fournisseur)), 1, 0, (GETPOST("type", 'alpha') == '' ? 1 : 0));
        //     // print '</td>';
        //     // print '<td>';
        //     // if (! empty($conf->fournisseur->enabled) && ! empty($user->rights->fournisseur->lire))
        //     // {
        //     // 	print $form->editfieldkey('SupplierCode', 'supplier_code', '', $object, 0);
        //     // }
        //     // print '</td><td>';
        //     // if (! empty($conf->fournisseur->enabled) && ! empty($user->rights->fournisseur->lire))
        //     // {
	    //     //     print '<table class="nobordernopadding"><tr><td>';
	    //     //     $tmpcode=$object->code_fournisseur;
	    //     //     if (empty($tmpcode) && ! empty($modCodeFournisseur->code_auto)) $tmpcode=$modCodeFournisseur->getNextValue($object, 1);
	    //     //     print '<input type="text" name="supplier_code" id="supplier_code" class="maxwidthonsmartphone" value="'.dol_escape_htmltag($tmpcode).'" maxlength="15">';
	    //     //     print '</td><td>';
	    //     //     $s=$modCodeFournisseur->getToolTip($langs, $object, 1);
	    //     //     print $form->textwithpicto('', $s, 1);
	    //     //     print '</td></tr></table>';
        //     // }
        //     // print '</td></tr>';
        // }

        // // Barcode
        // if (! empty($conf->barcode->enabled))
        // {
        //     print '<tr><td>'.$form->editfieldkey('Gencod', 'barcode', '', $object, 0).'</td>';
	    //     print '<td colspan="3"><input type="text" name="barcode" id="barcode" value="'.$object->barcode.'">';
        //     print '</td></tr>';
        // }

        
        

        // // Zip / Town
        // print '<tr><td>'.$form->editfieldkey('Zip', 'zipcode', '', $object, 0).'</td><td>';
        // print $formcompany->select_ziptown($object->zip, 'zipcode', array('town','selectcountry_id','state_id'), 0, 0, '', 'maxwidth100 quatrevingtpercent');
        // print '</td><td>'.$form->editfieldkey('Town', 'town', '', $object, 0).'</td><td>';
        // print $formcompany->select_ziptown($object->town, 'town', array('zipcode','selectcountry_id','state_id'), 0, 0, '', 'maxwidth100 quatrevingtpercent');
        // print '</td></tr>';

        // // Country
        // print '<tr><td>'.$form->editfieldkey('Country', 'selectcountry_id', '', $object, 0).'</td><td colspan="3" class="maxwidthonsmartphone">';
        // print $form->select_country((GETPOST('country_id')!=''?GETPOST('country_id'):$object->country_id));
        // if ($user->admin) print info_admin($langs->trans("YouCanChangeValuesForThisListFromDictionarySetup"), 1);
        // print '</td></tr>';

        // // State
        // if (empty($conf->global->SOCIETE_DISABLE_STATE))
        // {
        //     if(!empty($conf->global->MAIN_SHOW_REGION_IN_STATE_SELECT) && ($conf->global->MAIN_SHOW_REGION_IN_STATE_SELECT == 1 || $conf->global->MAIN_SHOW_REGION_IN_STATE_SELECT == 2))
        //     {
        //         print '<tr><td>'.$form->editfieldkey('Region-State', 'state_id', '', $object, 0).'</td><td colspan="3" class="maxwidthonsmartphone">';
        //     }
        //     else
        //     {
        //         print '<tr><td>'.$form->editfieldkey('State', 'state_id', '', $object, 0).'</td><td colspan="3" class="maxwidthonsmartphone">';
        //     }

        //     if ($object->country_id) print $formcompany->select_state($object->state_id, $object->country_code);
        //     else print $countrynotdefined;
        //     print '</td></tr>';
        // }

       

        // if (! empty($conf->socialnetworks->enabled))
        // {
        // 	// Skype
        // 	if (! empty($conf->global->SOCIALNETWORKS_SKYPE))
        // 	{
        // 		print '<tr><td>'.$form->editfieldkey('Skype', 'skype', '', $object, 0).'</td>';
		// 		print '<td colspan="3">';
		// 		print '<input type="text" name="skype" class="minwidth100" maxlength="80" id="skype" value="'.dol_escape_htmltag(GETPOSTISSET("skype")?GETPOST("skype", 'alpha'):$object->skype).'">';
		// 		print '</td></tr>';
        // 	}
        // 	// Twitter
        // 	if (! empty($conf->global->SOCIALNETWORKS_TWITTER))
        // 	{
        // 		print '<tr><td>'.$form->editfieldkey('Twitter', 'twitter', '', $object, 0).'</td>';
		// 		print '<td colspan="3">';
		// 		print '<input type="text" name="twitter" class="minwidth100" maxlength="80" id="twitter" value="'.dol_escape_htmltag(GETPOSTISSET("twitter")?GETPOST("twitter", 'alpha'):$object->twitter).'">';
		// 		print '</td></tr>';
        // 	}
        // 	// Facebook
        //     if (! empty($conf->global->SOCIALNETWORKS_FACEBOOK))
        //     {
        //         print '<tr><td>'.$form->editfieldkey('Facebook', 'facebook', '', $object, 0).'</td>';
        //         print '<td colspan="3">';
        //         print '<input type="text" name="facebook" class="minwidth100" maxlength="80" id="facebook" value="'.dol_escape_htmltag(GETPOSTISSET("facebook")?GETPOST("facebook", 'alpha'):$object->facebook).'">';
        //         print '</td></tr>';
        //     }
        //     // LinkedIn
        //     if (! empty($conf->global->SOCIALNETWORKS_LINKEDIN))
        //     {
        //         print '<tr><td>'.$form->editfieldkey('LinkedIn', 'linkedin', '', $object, 0).'</td>';
        //         print '<td colspan="3">';
        //         print '<input type="text" name="linkedin" class="minwidth100" maxlength="80" id="linkedin" value="'.dol_escape_htmltag(GETPOSTISSET("linkedin")?GETPOST("linkedin", 'alpha'):$object->linkedin).'">';
        //         print '</td></tr>';
        //     }
        // }

        // print '<td>';
        // print $form->select_company("", 'socid', '(s.client = 1 OR s.client = 2 OR s.client = 3) AND status=1', 'SelectThirdParty', 0, 0, null, 0, 'minwidth300');
        // if (!empty($conf->global->RELOAD_PAGE_ON_CUSTOMER_CHANGE))
		// {
		// 	print '<script type="text/javascript">
		// 	$(document).ready(function() {
		// 		$("#socid").change(function() {
		// 			var socid = $(this).val();
		// 			// reload page
		// 			window.location.href = "'.$_SERVER["PHP_SELF"].'?action=create&socid="+socid+"&ref_client="+$("input[name=ref_client]").val();
		// 		});
		// 	});
		// 	</script>';
		// }
		
        // print '</td>';
        
        // print '<td>'.$form->editfieldkey('Fax', 'fax', '', $object, 0).'</td>';
	    // print '<td><input type="text" name="fax" id="fax" class="maxwidth100onsmartphone quatrevingtpercent" value="'.$object->fax.'"></td></tr>';

        // Prof ids
        // $i=1; $j=0;
        // while ($i <= 6)
        // {
        //     $idprof=$langs->transcountry('ProfId'.$i, $object->country_code);
        //     if ($idprof!='-')
        //     {
	    //         $key='idprof'.$i;

        //         if (($j % 2) == 0) print '<tr>';

        //         $idprof_mandatory ='SOCIETE_IDPROF'.($i).'_MANDATORY';
        //         print '<td>'.$form->editfieldkey($idprof, $key, '', $object, 0, 'string', '', (empty($conf->global->$idprof_mandatory)?0:1)).'</td><td>';

        //         print $formcompany->get_input_id_prof($i, $key, $object->$key, $object->country_code);
        //         print '</td>';
        //         if (($j % 2) == 1) print '</tr>';
        //         $j++;
        //     }
        //     $i++;
        // }
        // if ($j % 2 == 1) print '<td colspan="2"></td></tr>';

        // // Vat is used
        // print '<tr><td>'.$form->editfieldkey('VATIsUsed', 'assujtva_value', '', $object, 0).'</td>';
        // print '<td>';
        // print $form->selectyesno('assujtva_value', GETPOSTISSET('assujtva_value')?GETPOST('assujtva_value', 'int'):1, 1);     // Assujeti par defaut en creation
        // print '</td>';
        // print '<td class="nowrap">'.$form->editfieldkey('VATIntra', 'intra_vat', '', $object, 0).'</td>';
        // print '<td class="nowrap">';
        // $s = '<input type="text" class="flat maxwidthonsmartphone" name="tva_intra" id="intra_vat" maxlength="20" value="'.$object->tva_intra.'">';

        // if (empty($conf->global->MAIN_DISABLEVATCHECK) && isInEEC($object))
        // {
        //     $s.=' ';

        //     if (! empty($conf->use_javascript_ajax))
        //     {
        //     	$widthpopup = 600;
        //     	if (! empty($conf->dol_use_jmobile)) $widthpopup = 350;
        //     	$heightpopup = 400;
        //         print "\n";
        //         print '<script language="JavaScript" type="text/javascript">';
        //         print "function CheckVAT(a) {\n";
        //         print "newpopup('".DOL_URL_ROOT."/societe/checkvat/checkVatPopup.php?vatNumber='+a, '".dol_escape_js($langs->trans("VATIntraCheckableOnEUSite"))."', ".$widthpopup.", ".$heightpopup.");\n";
        //         print "}\n";
        //         print '</script>';
        //         print "\n";
        //         $s.='<a href="#" class="hideonsmartphone" onclick="javascript: CheckVAT(document.formsoc.tva_intra.value);">'.$langs->trans("VATIntraCheck").'</a>';
        //         $s = $form->textwithpicto($s, $langs->trans("VATIntraCheckDesc", $langs->transnoentitiesnoconv("VATIntraCheck")), 1);
        //     }
        //     else
        //     {
        //         $s.='<a href="'.$langs->transcountry("VATIntraCheckURL", $object->country_id).'" target="_blank">'.img_picto($langs->trans("VATIntraCheckableOnEUSite"), 'help').'</a>';
        //     }
        // }
        // print $s;
        // print '</td>';
        // print '</tr>';

        // Local Taxes
        //TODO: Place into a function to control showing by country or study better option
        // if($mysoc->localtax1_assuj=="1" && $mysoc->localtax2_assuj=="1")
        // {
        //     print '<tr><td>'.$langs->transcountry("LocalTax1IsUsed", $mysoc->country_code).'</td><td>';
        //     print $form->selectyesno('localtax1assuj_value', (isset($conf->global->THIRDPARTY_DEFAULT_USELOCALTAX1)?$conf->global->THIRDPARTY_DEFAULT_USELOCALTAX1:0), 1);
        //     print '</td><td>'.$langs->transcountry("LocalTax2IsUsed", $mysoc->country_code).'</td><td>';
        //     print $form->selectyesno('localtax2assuj_value', (isset($conf->global->THIRDPARTY_DEFAULT_USELOCALTAX2)?$conf->global->THIRDPARTY_DEFAULT_USELOCALTAX2:0), 1);
        //     print '</td></tr>';
        // }
        // elseif($mysoc->localtax1_assuj=="1")
        // {
        //     print '<tr><td>'.$langs->transcountry("LocalTax1IsUsed", $mysoc->country_code).'</td><td colspan="3">';
        //     print $form->selectyesno('localtax1assuj_value', (isset($conf->global->THIRDPARTY_DEFAULT_USELOCALTAX1)?$conf->global->THIRDPARTY_DEFAULT_USELOCALTAX1:0), 1);
        //     print '</td></tr>';
        // }
        // elseif($mysoc->localtax2_assuj=="1")
        // {
        //     print '<tr><td>'.$langs->transcountry("LocalTax2IsUsed", $mysoc->country_code).'</td><td colspan="3">';
        //     print $form->selectyesno('localtax2assuj_value', (isset($conf->global->THIRDPARTY_DEFAULT_USELOCALTAX2)?$conf->global->THIRDPARTY_DEFAULT_USELOCALTAX2:0), 1);
        //     print '</td></tr>';
        // }

        // // Type - Size
        // print '<tr><td>'.$form->editfieldkey('ThirdPartyType', 'typent_id', '', $object, 0).'</td><td class="maxwidthonsmartphone">'."\n";
        // $sortparam=(empty($conf->global->SOCIETE_SORT_ON_TYPEENT)?'ASC':$conf->global->SOCIETE_SORT_ON_TYPEENT); // NONE means we keep sort of original array, so we sort on position. ASC, means next function will sort on label.
        // print $form->selectarray("typent_id", $formcompany->typent_array(0), $object->typent_id, 0, 0, 0, '', 0, 0, 0, $sortparam);
        // if ($user->admin) print ' '.info_admin($langs->trans("YouCanChangeValuesForThisListFromDictionarySetup"), 1);
        // print '</td>';
        // print '<td>'.$form->editfieldkey('Staff', 'effectif_id', '', $object, 0).'</td><td class="maxwidthonsmartphone">';
        // print $form->selectarray("effectif_id", $formcompany->effectif_array(0), $object->effectif_id);
        // if ($user->admin) print ' '.info_admin($langs->trans("YouCanChangeValuesForThisListFromDictionarySetup"), 1);
        // print '</td></tr>';

        // // Legal Form
        // print '<tr><td>'.$form->editfieldkey('JuridicalStatus', 'forme_juridique_code', '', $object, 0).'</td>';
        // print '<td colspan="3" class="maxwidthonsmartphone">';
        // if ($object->country_id)
        // {
        //     print $formcompany->select_juridicalstatus($object->forme_juridique_code, $object->country_code, '', 'forme_juridique_code');
        // }
        // else
        // {
        //     print $countrynotdefined;
        // }
        // print '</td></tr>';

        // // Capital
        // print '<tr><td>'.$form->editfieldkey('Capital', 'capital', '', $object, 0).'</td>';
	    // print '<td colspan="3"><input type="text" name="capital" id="capital" size="10" value="'.$object->capital.'"> ';
        // print '<span class="hideonsmartphone">'.$langs->trans("Currency".$conf->currency).'</span></td></tr>';

        // if (! empty($conf->global->MAIN_MULTILANGS))
        // {
        //     print '<tr><td>'.$form->editfieldkey('DefaultLang', 'default_lang', '', $object, 0).'</td><td colspan="3" class="maxwidthonsmartphone">'."\n";
        //     print $formadmin->select_language(GETPOST('default_lang', 'alpha')?GETPOST('default_lang', 'alpha'):($object->default_lang?$object->default_lang:''), 'default_lang', 0, 0, 1, 0, 0, 'maxwidth200onsmartphone');
        //     print '</td>';
        //     print '</tr>';
        // }

		// // Incoterms
		// if (!empty($conf->incoterm->enabled))
		// {
		// 	print '<tr>';
		// 	print '<td>'.$form->editfieldkey('IncotermLabel', 'incoterm_id', '', $object, 0).'</td>';
	    //     print '<td colspan="3" class="maxwidthonsmartphone">';
	    //     print $form->select_incoterms((!empty($object->fk_incoterms) ? $object->fk_incoterms : ''), (!empty($object->location_incoterms)?$object->location_incoterms:''));
		// 	print '</td></tr>';
		// }

		// // Categories
		// if (! empty($conf->categorie->enabled)  && ! empty($user->rights->categorie->lire))
		// {
		// 	$langs->load('categories');

		// 	// Customer
		// 	//if ($object->prospect || $object->client || (! $object->fournisseur && ! empty($conf->global->THIRDPARTY_CAN_HAVE_CATEGORY_EVEN_IF_NOT_CUSTOMER_PROSPECT_SUPPLIER))) {
		// 	print '<tr class="visibleifcustomer"><td class="toptd">' . $form->editfieldkey('CustomersProspectsCategoriesShort', 'custcats', '', $object, 0) . '</td><td colspan="3">';
		// 	$cate_arbo = $form->select_all_categories(Categorie::TYPE_CUSTOMER, null, 'parent', null, null, 1);
		// 	print $form->multiselectarray('custcats', $cate_arbo, GETPOST('custcats', 'array'), null, null, null, null, "90%");
		// 	print "</td></tr>";
		// 	//}

		// 	// Supplier
		// 	//if ($object->fournisseur) {
		// 	print '<tr class="visibleifsupplier"><td class="toptd">' . $form->editfieldkey('SuppliersCategoriesShort', 'suppcats', '', $object, 0) . '</td><td colspan="3">';
		// 	$cate_arbo = $form->select_all_categories(Categorie::TYPE_SUPPLIER, null, 'parent', null, null, 1);
		// 	print $form->multiselectarray('suppcats', $cate_arbo, GETPOST('suppcats', 'array'), null, null, null, null, "90%");
		// 	print "</td></tr>";
		// 	//}
		// }

		// // Multicurrency
		// if (! empty($conf->multicurrency->enabled))
		// {
		// 	print '<tr>';
		// 	print '<td>'.$form->editfieldkey('Currency', 'multicurrency_code', '', $object, 0).'</td>';
	    //     print '<td colspan="3" class="maxwidthonsmartphone">';
	    //     print $form->selectMultiCurrency(($object->multicurrency_code ? $object->multicurrency_code : $conf->currency), 'multicurrency_code', 1);
		// 	print '</td></tr>';
		// }

        // // Other attributes
        // $parameters=array('colspan' => ' colspan="3"', 'colspanvalue' => '3');
        // $reshook=$hookmanager->executeHooks('formObjectOptions', $parameters, $object, $action);    // Note that $action and $object may have been modified by hook
        // print $hookmanager->resPrint;
        // if (empty($reshook))
        // {
        // 	print $object->showOptionals($extrafields, 'edit');
        // }

		// Assign a sale representative
		// print '<tr>';
		// print '<td>'.$form->editfieldkey('AllocateCommercial', 'commercial_id', '', $object, 0).'</td>';
		// print '<td colspan="3" class="maxwidthonsmartphone">';
		// $userlist = $form->select_dolusers('', '', 0, null, 0, '', '', 0, 0, 0, '', 0, '', '', 0, 1);
		// // Note: If user has no right to "see all thirdparties", we for selection of sale representative to him, so after creation he can see the record.
		// $selected = (count(GETPOST('commercial', 'array')) > 0 ? GETPOST('commercial', 'array') : (GETPOST('commercial', 'int') > 0 ? array(GETPOST('commercial', 'int')) : (empty($user->rights->societe->client->voir)?array($user->id):array())));
		// print $form->multiselectarray('commercial', $userlist, $selected, null, null, null, null, "90%");
		// print '</td></tr>';

        

        print '</table>'."\n";

        dol_fiche_end();

        print '<div class="center">';
        print '<input type="submit" class="button" name="create" value="'.$langs->trans('AddThirdParty').'">';
        if (! empty($backtopage))
        {
            print ' &nbsp; &nbsp; ';
            print '<input type="submit" class="button" name="cancel" value="'.$langs->trans('Cancel').'">';
        }
        else
        {
            print ' &nbsp; &nbsp; ';
            print '<input type="button" class="button" value="' . $langs->trans("Cancel") . '" onClick="javascript:history.go(-1)">';
        }
        print '</div>'."\n";

        print '</form>'."\n";
    }
    elseif ($action == 'edit')
    {
        //print load_fiche_titre($langs->trans("EditCompany"));

        if ($socid)
        {
        	$res=$object->fetch_optionals();
            //if ($res < 0) { dol_print_error($db); exit; }

	        $head = societe_prepare_head($object);

            // Load object modCodeTiers
            $module=(! empty($conf->global->SOCIETE_CODECLIENT_ADDON)?$conf->global->SOCIETE_CODECLIENT_ADDON:'mod_codeclient_leopard');
            if (substr($module, 0, 15) == 'mod_codeclient_' && substr($module, -3) == 'php')
            {
                $module = substr($module, 0, dol_strlen($module)-4);
            }
            $dirsociete=array_merge(array('/core/modules/societe/'), $conf->modules_parts['societe']);
            foreach ($dirsociete as $dirroot)
            {
                $res=dol_include_once($dirroot.$module.'.php');
                if ($res) break;
            }
            $modCodeClient = new $module($db);
            // We verified if the tag prefix is used
            if ($modCodeClient->code_auto)
            {
                $prefixCustomerIsUsed = $modCodeClient->verif_prefixIsUsed();
            }
            $module=$conf->global->SOCIETE_CODECLIENT_ADDON;
            if (substr($module, 0, 15) == 'mod_codeclient_' && substr($module, -3) == 'php')
            {
                $module = substr($module, 0, dol_strlen($module)-4);
            }
            $dirsociete=array_merge(array('/core/modules/societe/'), $conf->modules_parts['societe']);
            foreach ($dirsociete as $dirroot)
            {
                $res=dol_include_once($dirroot.$module.'.php');
                if ($res) break;
            }
            $modCodeFournisseur = new $module($db);
            // On verifie si la balise prefix est utilisee
            if ($modCodeFournisseur->code_auto)
            {
                $prefixSupplierIsUsed = $modCodeFournisseur->verif_prefixIsUsed();
            }

			$object->oldcopy = clone $object;

            if (GETPOSTISSET('name'))
            {
                // We overwrite with values if posted
                $object->name					= GETPOST('name', 'alpha');
                $object->prefix_comm			= GETPOST('prefix_comm', 'alpha');
                $object->client					= GETPOST('client', 'int');
                $object->code_client			= GETPOST('customer_code', 'alpha');
                $object->fournisseur			= GETPOST('fournisseur', 'int');
                $object->code_fournisseur		= GETPOST('supplier_code', 'alpha');
                $object->address				= GETPOST('address', 'alpha');
                $object->zip					= GETPOST('zipcode', 'alpha');
                $object->town					= GETPOST('town', 'alpha');
                $object->country_id				= GETPOST('country_id')?GETPOST('country_id', 'int'):$mysoc->country_id;
                $object->state_id				= GETPOST('state_id', 'int');
                $object->skype					= GETPOST('skype', 'alpha');
                $object->twitter				= GETPOST('twitter', 'alpha');
                $object->facebook				= GETPOST('facebook', 'alpha');
                $object->linkedin				= GETPOST('linkedin', 'alpha');
                $object->phone					= GETPOST('phone', 'alpha');
                $object->fax					= GETPOST('fax', 'alpha');
                $object->email					= GETPOST('email', 'custom', 0, FILTER_SANITIZE_EMAIL);
                $object->url					= GETPOST('url', 'custom', 0, FILTER_SANITIZE_URL);
                $object->capital				= GETPOST('capital', 'alpha');
                $object->idprof1				= GETPOST('idprof1', 'alpha');
                $object->idprof2				= GETPOST('idprof2', 'alpha');
                $object->idprof3				= GETPOST('idprof3', 'alpha');
                $object->idprof4				= GETPOST('idprof4', 'alpha');
                $object->idprof5				= GETPOST('idprof5', 'alpha');
                $object->idprof6				= GETPOST('idprof6', 'alpha');
                $object->typent_id				= GETPOST('typent_id', 'int');
                $object->effectif_id			= GETPOST('effectif_id', 'int');
                $object->barcode				= GETPOST('barcode', 'alpha');
                $object->forme_juridique_code	= GETPOST('forme_juridique_code', 'int');
                $object->default_lang			= GETPOST('default_lang', 'alpha');

                $object->tva_assuj				= GETPOST('assujtva_value', 'int');
                $object->tva_intra				= GETPOST('tva_intra', 'alpha');
                $object->status					= GETPOST('status', 'int');

                //mis campos
                $object->tiposangre				= GETPOST('tiposangre', 'alpha');
                $object->genero					= GETPOST('genero', 'alpha');
                $object->nacimiento				= GETPOST('nacimiento', 'alpha');
                $object->civil					= GETPOST('civil', 'alpha');
                $object->edad					= GETPOST('edad', 'int');
                $object->ocupacion				= GETPOST('ocupacion', 'alpha');
                $object->contacto				= GETPOST('contacto', 'alpha');
                $object->parentesco				= GETPOST('parentesco', 'alpha');
                $object->observaciones			= GETPOST('observaciones', 'alpha');

                // Webservices url/key
                $object->webservices_url        = GETPOST('webservices_url', 'custom', 0, FILTER_SANITIZE_URL);
                $object->webservices_key        = GETPOST('webservices_key', 'san_alpha');

				//Incoterms
				if (!empty($conf->incoterm->enabled))
				{
					$object->fk_incoterms			= GETPOST('incoterm_id', 'int');
					$object->location_incoterms		= GETPOST('lcoation_incoterms', 'alpha');
				}

                //Local Taxes
                $object->localtax1_assuj		= GETPOST('localtax1assuj_value');
                $object->localtax2_assuj		= GETPOST('localtax2assuj_value');

                $object->localtax1_value		=GETPOST('lt1');
                $object->localtax2_value		=GETPOST('lt2');

                // We set country_id, and country_code label of the chosen country
                if ($object->country_id > 0)
                {
                	$tmparray=getCountry($object->country_id, 'all');
                    $object->country_code	= $tmparray['code'];
                    $object->country		= $tmparray['label'];
                }
            }

            if($object->localtax1_assuj==0){
            	$sub=0;
            }else{$sub=1;}
            if($object->localtax2_assuj==0){
            	$sub2=0;
            }else{$sub2=1;}

            if ($conf->use_javascript_ajax)
            {
            	print "\n".'<script type="text/javascript">';
            	print '$(document).ready(function () {
    			var val='.$sub.';
    			var val2='.$sub2.';
    			if("#localtax1assuj_value".value==undefined){
    				if(val==1){
    					$(".cblt1").show();
    				}else{
    					$(".cblt1").hide();
    				}
    			}
    			if("#localtax2assuj_value".value==undefined){
    				if(val2==1){
    					$(".cblt2").show();
    				}else{
    					$(".cblt2").hide();
    				}
    			}
    			$("#localtax1assuj_value").change(function() {
               		var value=document.getElementById("localtax1assuj_value").value;
    				if(value==1){
    					$(".cblt1").show();
    				}else{
    					$(".cblt1").hide();
    				}
    			});
    			$("#localtax2assuj_value").change(function() {
    				var value=document.getElementById("localtax2assuj_value").value;
    				if(value==1){
    					$(".cblt2").show();
    				}else{
    					$(".cblt2").hide();
    				}
    			});

				init_customer_categ();
	  			$("#customerprospect").change(function() {
					init_customer_categ();
				});
       			function init_customer_categ() {
					console.log("is customer or prospect = "+jQuery("#customerprospect").val());
					if (jQuery("#customerprospect").val() == 0 && (jQuery("#fournisseur").val() == 0 || '.(empty($conf->global->THIRDPARTY_CAN_HAVE_CATEGORY_EVEN_IF_NOT_CUSTOMER_PROSPECT_SUPPLIER)?'1':'0').'))
					{
						jQuery(".visibleifcustomer").hide();
					}
					else
					{
						jQuery(".visibleifcustomer").show();
					}
				}

				init_supplier_categ();
	  			$("#fournisseur").change(function() {
					init_supplier_categ();
				});
       			function init_supplier_categ() {
					console.log("is supplier = "+jQuery("#fournisseur").val());
					if (jQuery("#fournisseur").val() == 0)
					{
						jQuery(".visibleifsupplier").hide();
					}
					else
					{
						jQuery(".visibleifsupplier").show();
					}
				};

       			$("#selectcountry_id").change(function() {
       				document.formsoc.action.value="edit";
      				document.formsoc.submit();
        			});

                })';
                print '</script>'."\n";
            }

            print '<form enctype="multipart/form-data" action="'.$_SERVER["PHP_SELF"].'?socid='.$object->id.'" method="post" name="formsoc">';
            print '<input type="hidden" name="action" value="update">';
            print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
            print '<input type="hidden" name="socid" value="'.$object->id.'">';
            print '<input type="hidden" name="entity" value="'.$object->entity.'">';
            if ($modCodeClient->code_auto || $modCodeFournisseur->code_auto) print '<input type="hidden" name="code_auto" value="1">';


            dol_fiche_head($head, 'card', $langs->trans("ThirdParty"), 0, 'company');

            print '<div class="fichecenter2">';
            print '<table class="border" width="100%">';

            // Ref/ID
			if (! empty($conf->global->MAIN_SHOW_TECHNICAL_ID))
			{
		        print '<tr><td class="titlefieldcreate">'.$langs->trans("ID").'</td><td colspan="3">';
            	print $object->ref;
            	print '</td></tr>';
			}

			// Logo
            print '<tr class="hideonsmartphone">';
            print '<td colspan="3">';
            //if ($object->logo) print $form->showphoto('societe', $object);
            $caneditfield=1;
            if ($caneditfield)
            {
                //if ($object->logo) print "<br>\n";
                print '<table class="nobordernopadding">';
                if ($object->logo) print '<tr><td><input type="checkbox" class="flat photodelete" name="deletephoto" id="photodelete"> '.$langs->trans("Delete").'<br><br></td></tr>';
                //print '<tr><td>'.$langs->trans("PhotoFile").'</td></tr>';
               print ' <label for="photoinput">
                <img style="width:150px;height:150px;border-radius:150px;background:#9d9d9c;"  id="imagen" src="/aaa/dolibarr/htdocs/viewimage.php?modulepart=societe&entity=1&file=27%2F%2Flogos%2F'.$object->logo.'&cache=0"/>
                </label>

                <input style="display: none;" name="photo" id="photoinput"  type="file" />';

                 print '<script type="text/javascript">
            function init() {
              var inputFile = document.getElementById("photoinput");
              photoinput.addEventListener("change", mostrarImagen, false);
            }           

            function mostrarImagen(event) {
              var file = event.target.files[0];
              var reader = new FileReader();
              reader.onload = function(event) {
                var img = document.getElementById("imagen");
                img.src= event.target.result;
              }
              reader.readAsDataURL(file);
            }           

            window.addEventListener("load", init, false);
            </script>';
                print '</table>';
            }
            print '</td>';
            print '</tr>';

            // Name
            print '<tr><td class="titlefieldcreate">'.$form->editfieldkey('Nombre del paciente', 'name', '', $object, 0, 'string', '', 1).'</td>';
	        print '<td colspan="3"><input type="text" class="minwidth300" maxlength="128" name="name" id="name" value="'.dol_escape_htmltag($object->name).'" autofocus="autofocus"></td></tr>';

	         // Prospect/Customer
            print '<tr><td>'.$form->editfieldkey('Paciente potencial / Paciente	', 'customerprospect', '', $object, 0, 'string', '', 1).'</td>';
	        print '<td class="maxwidthonsmartphone">';
	        print $formcompany->selectProspectCustomerType($object->client);
            print '</td>';
            print '<td class="fieldrequired">'.$form->editfieldkey('ID Paciente	', 'customer_code', '', $object, 0).'</td><td>';

            print '<table class="nobordernopadding"><tr><td>';
            if ((!$object->code_client || $object->code_client == -1) && $modCodeClient->code_auto)
            {
                $tmpcode=$object->code_client;
                if (empty($tmpcode) && ! empty($object->oldcopy->code_client)) $tmpcode=$object->oldcopy->code_client; // When there is an error to update a thirdparty, the number for supplier and customer code is kept to old value.
                if (empty($tmpcode) && ! empty($modCodeClient->code_auto)) $tmpcode=$modCodeClient->getNextValue($object, 0);
                print '<input type="text" name="customer_code" id="customer_code" size="16" value="'.dol_escape_htmltag($tmpcode).'" maxlength="15">';
            }
            elseif ($object->codeclient_modifiable())
            {
            	print '<input type="text" name="customer_code" id="customer_code" size="16" value="'.dol_escape_htmltag($object->code_client).'" maxlength="15">';
            }
            else
            {
                print $object->code_client;
                print '<input type="hidden" name="customer_code" value="'.dol_escape_htmltag($object->code_client).'">';
            }
            print '</td><td>';
            $s=$modCodeClient->getToolTip($langs, $object, 0);
            print $form->textwithpicto('', $s, 1);
            print '</td></tr></table>';

            print '</td></tr>';


            // Status
            print '<tr><td class="fieldrequired">'.$form->editfieldkey('Status', 'status', '', $object, 0).'</td><td colspan="3">';
            print $form->selectarray('status', array('0'=>$langs->trans('ActivityCeased'),'1'=>$langs->trans('InActivity')), $object->status);
            print '</td></tr>';

            // Address
            print '<tr><td class="tdtop fieldrequired">'.$form->editfieldkey('Address', 'address', '', $object, 0).'</td>';
	        print '<td colspan="3"><textarea name="address" id="address" class="quatrevingtpercent" rows="3" wrap="soft">';
            print $object->address;
            print '</textarea></td></tr>';

             // // EMail / Web
            print '<tr><td class="fieldrequired">'.$form->editfieldkey('EMail', 'email', '', $object, 0, 'string', '', (! empty($conf->global->SOCIETE_EMAIL_MANDATORY))).'</td>';
	        print '<td colspan="3"><input type="text" name="email" id="email" size="32" value="'.$object->email.'"></td></tr>';

 			 // Phone / Fax
            print '<tr><td class="fieldrequired">'.$form->editfieldkey('Phone', 'phone', '', $object, 0).'</td>';
	        print '<td><input type="text" name="phone" id="phone" class="maxwidth100onsmartphone quatrevingtpercent" value="'.$object->phone.'"></td></tr>';
	        
	        //genero
             print '<tr><td class="fieldrequired">'.$form->editfieldkey('Genero', 'genero', '', $object, 0).'</td><td colspan="3">';
             print $form->selectarray('genero', array("$object->genero"=>$langs->trans("$object->genero"),'Masculino'=>$langs->trans('Masculino'),'Femenino'=>$langs->trans('Femenino')), 1);
             print '</td></tr>';  


             // Fecha de nacimiento
             print '<tr><td class="fieldrequired">'.$form->editfieldkey('Fecha de nacimiento', 'nacimiento', '', $object, 0).'</td>';
             print '<td><input type="date" name="nacimiento" id="nacimiento"  value="'.$object->nacimiento.'"></td>';
     		
     			print '<script type="text/javascript">
     			$(function(){
                 $("#nacimiento").on("change", calcularEdad);
             });
             
             function calcularEdad() {
                 
                 fecha = $(this).val();
                 var hoy = new Date();
                 var cumpleanos = new Date(fecha);
                 var edad = hoy.getFullYear() - cumpleanos.getFullYear();
                 var m = hoy.getMonth() - cumpleanos.getMonth();     

                 if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
                     edad--;
                 }
                 $("#edad").val(edad);
             }
     			</script>';
     		
             // Edad
             print '<tr><td class="fieldrequired">'.$form->editfieldkey('Edad', 'edad', '', $object, 0).'</td>';
             print '<td><input type="text" name="edad" id="edad" class="maxwidth100onsmartphone quatrevingtpercent" value="'.$object->edad.'"  readonly="readonly"></td></tr>';     

             // sangre
             print '<tr><td class="fieldrequired">'.$form->editfieldkey('Grupo sanguineo', 'tiposangre', '', $object, 0).'</td>';
             print '<td><input type="text" name="tiposangre" id="tiposangre" class="maxwidth100onsmartphone quatrevingtpercent" value="'.$object->tiposangre.'"></td>';     

             // Estado civil
             print '<tr><td class="fieldrequired">'.$form->editfieldkey('Estado civil', 'civil', '', $object, 0).'</td>';
             print '<td><input type="text" name="civil" id="civil" class="maxwidth100onsmartphone quatrevingtpercent" value="'.$object->civil.'"></td>';     

             // Ocupacion
             print '<tr><td class="fieldrequired">'.$form->editfieldkey('Ocupacion', 'ocupacion', '', $object, 0).'</td>';
             print '<td><input type="text" name="ocupacion" id="ocupacion" class="maxwidth100onsmartphone quatrevingtpercent" value="'.$object->ocupacion.'"></td>';     

             //familiares     

     	    print '<tr><td class="titlefieldcreate fieldrequired">' . $langs->trans("") . '</td><td>' . $langs->trans("Seleccione al familiar") . '</td></tr>';
     	
             print '<td class="fieldrequired">' . $langs->trans('Familiar registrado') . '</td>';     

              print '<td>';
             print $form->select_company($soc->id, 'contacto', '((s.client = 1 OR s.client = 3) AND s.status=1)', 'SelectThirdParty', 0, 0, null, 0, 'minwidth300');
             // Option to reload page to retrieve customer informations. Note, this clear other input
             if (!empty($conf->global->RELOAD_PAGE_ON_CUSTOMER_CHANGE))
             {
                 print '<script type="text/javascript">
                 $(document).ready(function() {
                     $("#contacto").change(function() {
                         var contacto = $(this).val();
                         var fac_rec = $(\'#fac_rec\').val();
                         // reload page
                         window.location.href = "'.$_SERVER["PHP_SELF"].'?action=create&contacto="+contacto+"&fac_rec="+fac_rec;
                     });
                 });
                 </script>';
             }
             print '</td>';     

             // parentezco
             print '<td class="fieldrequired">'.$form->editfieldkey('Parentesco', 'parentesco', '', $object, 0).'</td>';
             print '<td><input type="text" name="parentesco" id="parentesco" class="maxwidth100onsmartphone quatrevingtpercent" value="'.$object->parentesco.'"></td>';     

             // Observación
             print '<tr><td class="fieldrequired">'.$form->editfieldkey('Observación / Nota', 'observaciones', '', $object, 0).'</td>';
             print '<td colspan="3"><textarea name="observaciones" id="observaciones" class="quatrevingtpercent" rows="'.ROWS_2.'" wrap="soft">';
             print $object->observaciones;
             print '</textarea></td></tr>';


	           //          // Prefix
            // if (! empty($conf->global->SOCIETE_USEPREFIX))  // Old not used prefix field
            // {
            //     print '<tr><td class="fieldrequired">'.$form->editfieldkey('ID Paciente', 'prefix', '', $object, 0).'</td><td colspan="3">';
            //     // It does not change the prefix mode using the auto numbering prefix
            //     if (($prefixCustomerIsUsed || $prefixSupplierIsUsed) && $object->prefix_comm)
            //     {
            //         print '<input type="hidden" name="prefix_comm" value="'.dol_escape_htmltag($object->prefix_comm).'">';
            //         print $object->prefix_comm;
            //     }
            //     else
            //     {
            //         print '<input type="text" size="5" maxlength="5" name="prefix_comm" id="prefix" value="'.dol_escape_htmltag($object->prefix_comm).'">';
            //     }
            //     print '</td>';
            // }

	        // Alias names (commercial, trademark or alias names)
	        // print '<tr id="name_alias"><td><label for="name_alias_input">'.$langs->trans('AliasNames').'</label></td>';
	        // print '<td colspan="3"><input type="text" class="minwidth300" name="name_alias" id="name_alias_input" value="'.dol_escape_htmltag($object->name_alias).'"></td></tr>';

            // // Supplier
            // if ((! empty($conf->fournisseur->enabled) && ! empty($user->rights->fournisseur->lire))
            // 	|| (! empty($conf->supplier_proposal->enabled) && ! empty($user->rights->supplier_proposal->lire)))
            // {
            //     print '<tr>';
            //     print '<td>'.$form->editfieldkey('Supplier', 'fournisseur', '', $object, 0, 'string', '', 1).'</td><td class="maxwidthonsmartphone">';
            //     print $form->selectyesno("fournisseur", $object->fournisseur, 1);
            //     print '</td>';
            //     print '<td>';
            //     if (! empty($conf->fournisseur->enabled) && ! empty($user->rights->fournisseur->lire))
            //     {
            //     	print $form->editfieldkey('SupplierCode', 'supplier_code', '', $object, 0);
            //     }
            //     print '</td><td>';
            //     if (! empty($conf->fournisseur->enabled) && ! empty($user->rights->fournisseur->lire))
            //     {
	           //      print '<table class="nobordernopadding"><tr><td>';
	           //      if ((!$object->code_fournisseur || $object->code_fournisseur == -1) && $modCodeFournisseur->code_auto)
	           //      {
	           //          $tmpcode=$object->code_fournisseur;
	           //          if (empty($tmpcode) && ! empty($object->oldcopy->code_fournisseur)) $tmpcode=$object->oldcopy->code_fournisseur; // When there is an error to update a thirdparty, the number for supplier and customer code is kept to old value.
	           //          if (empty($tmpcode) && ! empty($modCodeFournisseur->code_auto)) $tmpcode=$modCodeFournisseur->getNextValue($object, 1);
	           //          print '<input type="text" name="supplier_code" id="supplier_code" size="16" value="'.dol_escape_htmltag($tmpcode).'" maxlength="15">';
	           //      }
	           //      elseif ($object->codefournisseur_modifiable())
	           //      {
	           //          print '<input type="text" name="supplier_code" id="supplier_code" size="16" value="'.$object->code_fournisseur.'" maxlength="15">';
	           //      }
	           //      else
	           //    {
	           //          print $object->code_fournisseur;
	           //          print '<input type="hidden" name="supplier_code" value="'.$object->code_fournisseur.'">';
	           //      }
	           //      print '</td><td>';
	           //      $s=$modCodeFournisseur->getToolTip($langs, $object, 1);
	           //      print $form->textwithpicto('', $s, 1);
	           //      print '</td></tr></table>';
            //     }
            //     print '</td></tr>';
            // }

            // // Barcode
            // if (! empty($conf->barcode->enabled))
            // {
            //     print '<tr><td class="tdtop">'.$form->editfieldkey('Gencod', 'barcode', '', $object, 0).'</td>';
	           //  print '<td colspan="3"><input type="text" name="barcode" id="barcode" value="'.$object->barcode.'">';
            //     print '</td></tr>';
            // }


            // // Zip / Town
            // print '<tr><td>'.$form->editfieldkey('Zip', 'zipcode', '', $object, 0).'</td><td>';
            // print $formcompany->select_ziptown($object->zip, 'zipcode', array('town', 'selectcountry_id', 'state_id'), 0, 0, '', 'maxwidth50onsmartphone');
            // print '</td><td>'.$form->editfieldkey('Town', 'town', '', $object, 0).'</td><td>';
            // print $formcompany->select_ziptown($object->town, 'town', array('zipcode', 'selectcountry_id', 'state_id'));
            // print '</td></tr>';

            // // Country
            // print '<tr><td>'.$form->editfieldkey('Country', 'selectcounty_id', '', $object, 0).'</td><td colspan="3">';
            // print $form->select_country((GETPOST('country_id')!=''?GETPOST('country_id'):$object->country_id), 'country_id');
            // if ($user->admin) print info_admin($langs->trans("YouCanChangeValuesForThisListFromDictionarySetup"), 1);
            // print '</td></tr>';

            // // State
            // if (empty($conf->global->SOCIETE_DISABLE_STATE))
            // {
            //     if(!empty($conf->global->MAIN_SHOW_REGION_IN_STATE_SELECT) && ($conf->global->MAIN_SHOW_REGION_IN_STATE_SELECT == 1 || $conf->global->MAIN_SHOW_REGION_IN_STATE_SELECT == 2))
            //     {
            //         print '<tr><td>'.$form->editfieldkey('Region-State', 'state_id', '', $object, 0).'</td><td colspan="3">';
            //     }
            //     else
            //     {
            //         print '<tr><td>'.$form->editfieldkey('State', 'state_id', '', $object, 0).'</td><td colspan="3">';
            //     }

            //     print $formcompany->select_state($object->state_id, $object->country_code);
            //     print '</td></tr>';
            // }

            //print '<tr><td>'.$form->editfieldkey('Web', 'url', '', $object, 0).'</td>';
	       // print '<td colspan="3"><input type="text" name="url" id="url" size="32" value="'.$object->url.'"></td></tr>';

	        // if (! empty($conf->socialnetworks->enabled))
	        // {
	        // 	// Skype
	        // 	if (! empty($conf->global->SOCIALNETWORKS_SKYPE))
	        // 	{
	        // 		print '<tr><td>'.$form->editfieldkey('Skype', 'skype', '', $object, 0).'</td>';
	        // 		print '<td colspan="3"><input type="text" name="skype" id="skype" value="'.$object->skype.'"></td></tr>';
	        // 	}
	        // 	// Twitter
	        // 	if (! empty($conf->global->SOCIALNETWORKS_TWITTER))
	        // 	{
	        // 		print '<tr><td>'.$form->editfieldkey('Twitter', 'twitter', '', $object, 0).'</td>';
	        // 		print '<td colspan="3"><input type="text" name="twitter" id="twitter" value="'.$object->twitter.'"></td></tr>';
	        // 	}
	        // 	// Facebook
	        // 	if (! empty($conf->global->SOCIALNETWORKS_FACEBOOK))
	        // 	{
	        // 		print '<tr><td>'.$form->editfieldkey('Facebook', 'facebook', '', $object, 0).'</td>';
	        // 		print '<td colspan="3"><input type="text" name="facebook" id="facebook" value="'.$object->facebook.'"></td></tr>';
	        // 	}
         //        // LinkedIn
         //        if (! empty($conf->global->SOCIALNETWORKS_LINKEDIN))
         //        {
         //            print '<tr><td>'.$form->editfieldkey('LinkedIn', 'linkedin', '', $object, 0).'</td>';
         //            print '<td colspan="3"><input type="text" name="linkedin" id="linkedin" value="'.$object->linkedin.'"></td></tr>';
         //        }
	        // }

           // print '<td>'.$form->editfieldkey('Fax', 'fax', '', $object, 0).'</td>';
	      //  print '<td><input type="text" name="fax" id="fax" class="maxwidth100onsmartphone quatrevingtpercent" value="'.$object->fax.'"></td></tr>';

            // // Prof ids
            // $i=1; $j=0;
            // while ($i <= 6)
            // {
            //     $idprof=$langs->transcountry('ProfId'.$i, $object->country_code);
            //     if ($idprof!='-')
            //     {
	           //      $key='idprof'.$i;

	           //      if (($j % 2) == 0) print '<tr>';

	           //      $idprof_mandatory ='SOCIETE_IDPROF'.($i).'_MANDATORY';
	           //      print '<td>'.$form->editfieldkey($idprof, $key, '', $object, 0, 'string', '', ! (empty($conf->global->$idprof_mandatory) || ! $object->isACompany())).'</td><td>';
	           //      print $formcompany->get_input_id_prof($i, $key, $object->$key, $object->country_code);
            //         print '</td>';
            //         if (($j % 2) == 1) print '</tr>';
            //         $j++;
            //     }
            //     $i++;
            // }
            // if ($j % 2 == 1) print '<td colspan="2"></td></tr>';

            // // VAT is used
            // print '<tr><td>'.$form->editfieldkey('VATIsUsed', 'assujtva_value', '', $object, 0).'</td><td colspan="3">';
            // print $form->selectyesno('assujtva_value', $object->tva_assuj, 1);
            // print '</td></tr>';

            // // Local Taxes
            // //TODO: Place into a function to control showing by country or study better option
            // if($mysoc->localtax1_assuj=="1" && $mysoc->localtax2_assuj=="1")
            // {
            //     print '<tr><td>'.$form->editfieldkey($langs->transcountry("LocalTax1IsUsed", $mysoc->country_code), 'localtax1assuj_value', '', $object, 0).'</td><td>';
            //     print $form->selectyesno('localtax1assuj_value', $object->localtax1_assuj, 1);
            //     if(! isOnlyOneLocalTax(1))
            //     {
            //         print '<span class="cblt1">     '.$langs->transcountry("Type", $mysoc->country_code).': ';
            //         $formcompany->select_localtax(1, $object->localtax1_value, "lt1");
            //         print '</span>';
            //     }
            //     print '</td>';

            //     print '<td>'.$form->editfieldkey($langs->transcountry("LocalTax2IsUsed", $mysoc->country_code), 'localtax2assuj_value', '', $object, 0).'</td><td>';
            //     print $form->selectyesno('localtax2assuj_value', $object->localtax2_assuj, 1);
            //     if  (! isOnlyOneLocalTax(2))
            //     {
            //         print '<span class="cblt2">     '.$langs->transcountry("Type", $mysoc->country_code).': ';
            //         $formcompany->select_localtax(2, $object->localtax2_value, "lt2");
            //         print '</span>';
            //     }
            //     print '</td></tr>';
            // }
            // elseif($mysoc->localtax1_assuj=="1" && $mysoc->localtax2_assuj!="1")
            // {
            //     print '<tr><td>'.$form->editfieldkey($langs->transcountry("LocalTax1IsUsed", $mysoc->country_code), 'localtax1assuj_value', '', $object, 0).'</td><td colspan="3">';
            //     print $form->selectyesno('localtax1assuj_value', $object->localtax1_assuj, 1);
            //     if(! isOnlyOneLocalTax(1))
            //     {
            //         print '<span class="cblt1">     '.$langs->transcountry("Type", $mysoc->country_code).': ';
            //         $formcompany->select_localtax(1, $object->localtax1_value, "lt1");
            //         print '</span>';
            //     }
            //     print '</td></tr>';
            // }
            // elseif($mysoc->localtax2_assuj=="1" && $mysoc->localtax1_assuj!="1")
            // {
            //     print '<tr><td>'.$form->editfieldkey($langs->transcountry("LocalTax2IsUsed", $mysoc->country_code), 'localtax2assuj_value', '', $object, 0).'</td><td colspan="3">';
            //     print $form->selectyesno('localtax2assuj_value', $object->localtax2_assuj, 1);
            //     if(! isOnlyOneLocalTax(2))
            //     {
            //         print '<span class="cblt2">     '.$langs->transcountry("Type", $mysoc->country_code).': ';
            //         $formcompany->select_localtax(2, $object->localtax2_value, "lt2");
            //         print '</span>';
            //     }
            //     print '</td></tr>';
            // }

            // // VAT Code
            // print '<tr><td>'.$form->editfieldkey('VATIntra', 'intra_vat', '', $object, 0).'</td>';
            // print '<td colspan="3">';
            // $s ='<input type="text" class="flat maxwidthonsmartphone" name="tva_intra" id="intra_vat" maxlength="20" value="'.$object->tva_intra.'">';

            // if (empty($conf->global->MAIN_DISABLEVATCHECK) && isInEEC($object))
            // {
            //     $s.=' &nbsp; ';

            //     if ($conf->use_javascript_ajax)
            //     {
            //     	$widthpopup = 600;
            //     	if (! empty($conf->dol_use_jmobile)) $widthpopup = 350;
            //     	$heightpopup = 400;
            //     	print "\n";
            //         print '<script language="JavaScript" type="text/javascript">';
            //         print "function CheckVAT(a) {\n";
            //         print "newpopup('".DOL_URL_ROOT."/societe/checkvat/checkVatPopup.php?vatNumber='+a,'".dol_escape_js($langs->trans("VATIntraCheckableOnEUSite"))."', ".$widthpopup.", ".$heightpopup.");\n";
            //         print "}\n";
            //         print '</script>';
            //         print "\n";
            //         $s.='<a href="#" class="hideonsmartphone" onclick="javascript: CheckVAT(document.formsoc.tva_intra.value);">'.$langs->trans("VATIntraCheck").'</a>';
            //         $s = $form->textwithpicto($s, $langs->trans("VATIntraCheckDesc", $langs->transnoentitiesnoconv("VATIntraCheck")), 1);
            //     }
            //     else
            //     {
            //         $s.='<a href="'.$langs->transcountry("VATIntraCheckURL", $object->country_id).'" class="hideonsmartphone" target="_blank">'.img_picto($langs->trans("VATIntraCheckableOnEUSite"), 'help').'</a>';
            //     }
            // }
            // print $s;
            // print '</td>';
            // print '</tr>';

            // // Type - Size
            // print '<tr><td>'.$form->editfieldkey('ThirdPartyType', 'typent_id', '', $object, 0).'</td><td class="maxwidthonsmartphone">';
            // print $form->selectarray("typent_id", $formcompany->typent_array(0), $object->typent_id, 0, 0, 0, '', 0, 0, 0, (empty($conf->global->SOCIETE_SORT_ON_TYPEENT)?'ASC':$conf->global->SOCIETE_SORT_ON_TYPEENT));
            // if ($user->admin) print info_admin($langs->trans("YouCanChangeValuesForThisListFromDictionarySetup"), 1);
            // print '</td>';
            // print '<td>'.$form->editfieldkey('Staff', 'effectif_id', '', $object, 0).'</td><td class="maxwidthonsmartphone">';
            // print $form->selectarray("effectif_id", $formcompany->effectif_array(0), $object->effectif_id);
            // if ($user->admin) print info_admin($langs->trans("YouCanChangeValuesForThisListFromDictionarySetup"), 1);
            // print '</td></tr>';

            // // Juridical type
            // print '<tr><td>'.$form->editfieldkey('JuridicalStatus', 'forme_juridique_code', '', $object, 0).'</td><td class="maxwidthonsmartphone" colspan="3">';
            // print $formcompany->select_juridicalstatus($object->forme_juridique_code, $object->country_code, '', 'forme_juridique_code');
            // print '</td></tr>';

            // // Capital
            // print '<tr><td>'.$form->editfieldkey('Capital', 'capital', '', $object, 0).'</td>';
	        // print '<td colspan="3"><input type="text" name="capital" id="capital" size="10" value="';
	        // print $object->capital != '' ? dol_escape_htmltag(price($object->capital)) : '';
	        // print '"> <font class="hideonsmartphone">'.$langs->trans("Currency".$conf->currency).'</font></td></tr>';

   //          // Default language
   //          if (! empty($conf->global->MAIN_MULTILANGS))
   //          {
   //              print '<tr><td>'.$form->editfieldkey('DefaultLang', 'default_lang', '', $object, 0).'</td><td colspan="3">'."\n";
   //              print $formadmin->select_language($object->default_lang, 'default_lang', 0, 0, 1);
   //              print '</td>';
   //              print '</tr>';
   //          }

   //          // Incoterms
   //          if (!empty($conf->incoterm->enabled))
   //          {
   //          	print '<tr>';
   //    				print '<td>'.$form->editfieldkey('IncotermLabel', 'incoterm_id', '', $object, 0).'</td>';
   //          	print '<td colspan="3" class="maxwidthonsmartphone">';
   //          	print $form->select_incoterms((!empty($object->fk_incoterms) ? $object->fk_incoterms : ''), (!empty($object->location_incoterms)?$object->location_incoterms:''));
   //          	print '</td></tr>';
   //          }

			// // Categories
			// if (! empty($conf->categorie->enabled)  && ! empty($user->rights->categorie->lire))
			// {
			// 	// Customer
			// 	print '<tr class="visibleifcustomer"><td>' . $form->editfieldkey('CustomersCategoriesShort', 'custcats', '', $object, 0) . '</td>';
			// 	print '<td colspan="3">';
			// 	$cate_arbo = $form->select_all_categories(Categorie::TYPE_CUSTOMER, null, null, null, null, 1);
			// 	$c = new Categorie($db);
			// 	$cats = $c->containing($object->id, Categorie::TYPE_CUSTOMER);
			// 	$arrayselected=array();
			// 	foreach ($cats as $cat) {
			// 		$arrayselected[] = $cat->id;
			// 	}
			// 	print $form->multiselectarray('custcats', $cate_arbo, $arrayselected, '', 0, '', 0, '90%');
			// 	print "</td></tr>";

			// 	// Supplier
			// 	print '<tr class="visibleifsupplier"><td>' . $form->editfieldkey('SuppliersCategoriesShort', 'suppcats', '', $object, 0) . '</td>';
			// 	print '<td colspan="3">';
			// 	$cate_arbo = $form->select_all_categories(Categorie::TYPE_SUPPLIER, null, null, null, null, 1);
			// 	$c = new Categorie($db);
			// 	$cats = $c->containing($object->id, Categorie::TYPE_SUPPLIER);
			// 	$arrayselected=array();
			// 	foreach ($cats as $cat) {
			// 		$arrayselected[] = $cat->id;
			// 	}
			// 	print $form->multiselectarray('suppcats', $cate_arbo, $arrayselected, '', 0, '', 0, '90%');
			// 	print "</td></tr>";
			// }

			// // Multicurrency
			// if (! empty($conf->multicurrency->enabled))
			// {
			// 	print '<tr>';
			// 	print '<td>'.$form->editfieldkey('Currency', 'multicurrency_code', '', $object, 0).'</td>';
		 //        print '<td colspan="3" class="maxwidthonsmartphone">';
		 //        print $form->selectMultiCurrency(($object->multicurrency_code ? $object->multicurrency_code : $conf->currency), 'multicurrency_code', 1);
			// 	print '</td></tr>';
			// }

   		//          // Other attributes
	    //          $parameters=array('colspan' => ' colspan="3"', 'colspanvalue' => '3');
	    		//          $reshook=$hookmanager->executeHooks('formObjectOptions', $parameters, $object, $action);    // Note that $action and $object may have been modified by hook
	    //          print $hookmanager->resPrint;
	    //          if (empty($reshook))
	    //          {
	    //          	print $object->showOptionals($extrafields, 'edit');
	    //          }	 

	    //          // Webservices url/key
	    //          if (!empty($conf->syncsupplierwebservices->enabled)) {
	    //              print '<tr><td>'.$form->editfieldkey('WebServiceURL', 'webservices_url', '', $object, 0).'</td>';
	    //              print '<td><input type="text" name="webservices_url" id="webservices_url" size="32" value="'.$object->webservices_url.'"></td>';
	    //              print '<td>'.$form->editfieldkey('WebServiceKey', 'webservices_key', '', $object, 0).'</td>';
	    //              print '<td><input type="text" name="webservices_key" id="webservices_key" size="32" value="'.$object->webservices_key.'"></td></tr>';
	    //          }


            // // Assign sale representative
            // print '<tr>';
            // print '<td>'.$form->editfieldkey('AllocateCommercial', 'commercial_id', '', $object, 0).'</td>';
            // print '<td colspan="3" class="maxwidthonsmartphone">';
            // $userlist = $form->select_dolusers('', '', 0, null, 0, '', '', 0, 0, 0, '', 0, '', '', 0, 1);
            // $arrayselected = GETPOST('commercial', 'array');
            // if (empty($arrayselected)) $arrayselected = $object->getSalesRepresentatives($user, 1);
            // print $form->multiselectarray('commercial', $userlist, $arrayselected, null, null, null, null, "90%");
            // print '</td></tr>';

            print '</table>';
            print '</div>';

	          dol_fiche_end();

            print '<div class="center">';
            print '<input type="submit" class="button" name="save" value="'.$langs->trans("Modificar").'">';
            print ' &nbsp; &nbsp; ';
            print '<input type="submit" class="button" name="cancel" value="'.$langs->trans("Cancel").'">';
            print '</div>';

            print '</form>';
        }
    }
    else
    {

        print '<div>
        <h2 readonly="readonly">'.$object->nom.'
        <button type="button" class="btn btn-success" onclick="crear();">Crear Cita</button>
        <button type="button" class="btn btn-info" onclick="mandaremail();">Enviar Email</button>
        </h2>
        </div>';

        $a=DOL_URL_ROOT.'/comm/action/card.php?action=create&socid='.$object->id;

        print '<script type="text/javascript">
                 function mandaremail(){
                         window.location.href = "?socid='.$object->id.'&action=presend&mode=init#formmailbeforetitle";
                     }
                     
                     function crear(){
                        window.location.href = "'.$a.'";
                    }
                 </script>';

                // comm/action/card.php?action=create&socid=4&backtopage=1&percentage=-1

        $head = societe_prepare_head($object);

        dol_fiche_head($head, 'card', $langs->trans("Paciente"), -1, 'company');

        dol_fiche_end();


        /*
         *  Actions
         */
        if ($action != 'presend')
        {
            // historial clinico

            print '<div class="tabsAction">'."\n";

            print '<button type="button" class="btn btn-dark">Imprimir</button></div>'."\n";
            
            print '
            <form>

            <div class="form-group row">
              <label for="motivo" class="col-sm-2 col-form-label">Motivo consulta</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="motivo" placeholder="">
              </div>
            </div>

            <div class="form-group row">
              <label for="enfermedadactual" class="col-sm-2 col-form-label">Enfermedad actual</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="enfermedadactual" placeholder="">
              </div>
            </div>

            <div class="form-group row">

                <label class="col-sm-12 col-form-label center">ANTECEDENTES</label>
            
            </div>


            <div class="form-group row">
              <label for="enfermedades" class="col-sm-2 col-form-label">Enfermedades</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="enfermedades" placeholder="">
              </div>
            </div>
            
            <div class="form-group row">
              <label for="antecedentes" class="col-sm-2 col-form-label">Antecedentes quirúrgicos</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="antecedentes" placeholder="">
              </div>
            </div>

            <div class="form-group row">
              <label for="alergias" class="col-sm-2 col-form-label">Alergias</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="alergias" placeholder="">
              </div>
            </div>

            <div class="form-group row">
              <label for="medicamentos" class="col-sm-2 col-form-label">Medicamentos</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="medicamentos" placeholder="">
              </div>
            </div>

            <div class="form-group row">
              <label for="habitos" class="col-sm-2 col-form-label">Hábitos</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="habitos" placeholder="">
              </div>
            </div>

            <div class="form-group row">
              <label for="antecedentesfam" class="col-sm-2 col-form-label">Antecedentes familiares</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="antecedentesfam" placeholder="">
              </div>
            </div>
            

            <div class="form-group row">
              <label for="fuma" class="col-sm-2 col-form-label">Fuma</label>
              <div class="col-sm-10">
              <label class="radio-inline"><input type="radio" name="fuma">Si</label>
              <label class="radio-inline"><input type="radio" name="fuma">No</label>
              </div>
            </div>

            <div class="form-group row">
              <label for="embarazo" class="col-sm-2 col-form-label">Embarazo</label>
              <div class="col-sm-10">
              <label class="radio-inline"><input type="radio" name="embarazo">Si</label>
              <label class="radio-inline"><input type="radio" name="embarazo">No</label>
              </div>
            </div>

            <div class="form-group row">

                <label class="col-sm-12 col-form-label center">ANÁLISIS FACIAL Y OCLUSAL</label>
            
            </div>


            <div class="form-group row">
              <label for="examenlateral" class="col-sm-2 col-form-label">Examen lateral</label>

              <div class="col-sm-3 center">
                <img style="width: 100px;height: 100px;background: #b4b5b5;">
              </div>

              <div class="col-sm-3 center">
                <img style="width: 100px;height: 100px;background: #b4b5b5;">
              </div>

              <div class="col-sm-3 center">
                <img style="width: 100px;height: 100px;background: #b4b5b5;">
              </div>

            </div>

            <div class="form-group row">
                <label for="sobremordida" class="col-sm-2 col-form-label">Sobremordida</label>
            
                <div class="col-sm-3 center">
                  <img style="width: 100px;height: 100px;background: #b4b5b5;">
                </div>
            
                <div class="col-sm-3 center">
                  <img style="width: 100px;height: 100px;background: #b4b5b5;">
                </div>
            
                <div class="col-sm-3 center">
                  <img style="width: 100px;height: 100px;background: #b4b5b5;">
                </div>

            </div>

            <div class="form-group row">
                <label for="cruzado" class="col-sm-2 col-form-label">Cruzado</label>
            
                <div class="col-sm-3 center">
                  <img style="width: 100px;height: 100px;background: #b4b5b5;">
                </div>
            
                <div class="col-sm-3 center">
                  <img style="width: 100px;height: 100px;background: #b4b5b5;">
                </div>
            
                <div class="col-sm-3 center">
                  <img style="width: 100px;height: 100px;background: #b4b5b5;">
                </div>

            </div>

            <div class="form-group row">
                <label for="Lineasonrisa" class="col-sm-2 col-form-label">Línea de sonrisa</label>
            
                <div class="col-sm-3 center">
                  <img style="width: 100px;height: 100px;background: #b4b5b5;">
                </div>
            
                <div class="col-sm-3 center">
                  <img style="width: 100px;height: 100px;background: #b4b5b5;">
                </div>
            
                <div class="col-sm-3 center">
                  <img style="width: 100px;height: 100px;background: #b4b5b5;">
                </div>

            </div>

            <div class="form-group row">

                <label class="col-sm-12 col-form-label center">LÍNEA MEDIA DENTAL</label>
            
            </div>

            <div class="form-group row">
              <label for="dentalsuperio" class="col-sm-2 col-form-label">Línea media dental superior</label>
                <select  id="dentalsuperio" class="col-sm-10">
                    <option>Seleccionar opcion</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
            </div>

            <div class="form-group row">
              <label for="milimetros" class="col-sm-2 col-form-label">Milímetros</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="milimetros" placeholder="">
              </div>
            </div>

            <div class="form-group row">
              <label for="dentalinferior" class="col-sm-2 col-form-label">Línea media dental inferior</label>
                <select  id="dentalinferior" class="col-sm-10">
                    <option>Seleccionar opcion</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
            </div>

            <div class="form-group row">
              <label for="milimetrosinf" class="col-sm-2 col-form-label">Milímetros</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="milimetrosinf" placeholder="">
              </div>
            </div>

            <div class="form-group row">

                <label class="col-sm-12 col-form-label center">APIÑAMIENTO</label>
            
            </div>

            <div class="form-group row">
              <label for="superior" class="col-sm-2 col-form-label">Superior</label>
                <select  id="superior" class="col-sm-10">
                    <option>Seleccionar opcion</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
            </div>

            <div class="form-group row">
              <label for="inferior" class="col-sm-2 col-form-label">Inferior</label>
                <select  id="inferior" class="col-sm-10">
                    <option>Seleccionar opcion</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
            </div>

            <div class="form-group row">
              <label for="observaciones" class="col-sm-2 col-form-label">Observaciones</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="observaciones" placeholder="">
              </div>
            </div>

            <div class="form-group row">

                <label class="col-sm-12 col-form-label center">ESTOMATOLÓGICO</label>
            
            </div>

            <div class="form-group row">

                <div class="col-sm-2"></div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <input type="checkbox" name="pisodelaboca" id="pisodelaboca" autocomplete="off" />
                        <label for="pisodelaboca" class="btn btn-default active">
                        Cambios en el piso de la boca
                        </label>
                    </div>
   
                    <div class="form-group">
                        <input type="checkbox" name="labios" id="labios" autocomplete="off" />
                            <label for="labios" class="btn btn-default active">
                                Labios
                            </label>
                    </div>
   
                    <div class="form-group">
                        <input type="checkbox" name="frenillo" id="frenillo" autocomplete="off" />
                            <label for="frenillo" class="btn btn-default active">
                                Frenillo
                            </label>
                    </div>
           
                    <div class="form-group">
                        <input type="checkbox" name="cambioesmalte" id="cambioesmalte" autocomplete="off" />
                            <label for="cambioesmalte" class="btn btn-default active">
                                Cambio en el esmalte y la dentina
                            </label>
                    </div> 

                </div>

                <div class="col-sm-4">

                    <div class="form-group">
                        <input type="checkbox" name="mejillas" id="mejillas" autocomplete="off" />
                        <label for="mejillas" class="btn btn-default active">
                            Las mejillas
                        </label>
                    </div>
   
                    <div class="form-group">
                        <input type="checkbox" name="paladar" id="paladar" autocomplete="off" />
                            <label for="paladar" class="btn btn-default active">
                                Paladar
                            </label>
                    </div>
   
                    <div class="form-group">
                        <input type="checkbox" name="lengua" id="lengua" autocomplete="off" />
                            <label for="lengua" class="btn btn-default active">
                                Lengua
                            </label>
                    </div>
           
                    <div class="form-group">
                        <input type="checkbox" name="observaciones" id="observaciones" autocomplete="off" />
                            <label for="observaciones" class="btn btn-default active">
                                Observaciones
                            </label>
                    </div> 

                </div>

                <div class="col-sm-2"></div>

            </div>


            <div class="form-group row">

                <label class="col-sm-12 col-form-label center">ANÁLISIS FUNCIONAL</label>
            
            </div>


            <div class="form-group row">

                <div class="col-sm-2"></div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <input type="checkbox" name="digital" id="digital" autocomplete="off" />
                        <label for="digital" class="btn btn-default active">
                        Succión digital
                        </label>
                    </div>
   
                    <div class="form-group">
                        <input type="checkbox" name="onychophogia" id="onychophogia" autocomplete="off" />
                            <label for="onychophogia" class="btn btn-default active">
                            Onychophogia
                            </label>
                    </div>
   
                    <div class="form-group">
                        <input type="checkbox" name="respiracion" id="respiracion" autocomplete="off" />
                            <label for="respiracion" class="btn btn-default active">
                            Respiración oral
                            </label>
                    </div>
           
                    <div class="form-group">
                        <input type="checkbox" name="masticar" id="masticar" autocomplete="off" />
                            <label for="masticar" class="btn btn-default active">
                            Masticar la enfermedad
                            </label>
                    </div> 

                </div>

                <div class="col-sm-4">

                    <div class="form-group">
                        <input type="checkbox" name="deglucion" id="deglucion" autocomplete="off" />
                        <label for="deglucion" class="btn btn-default active">
                        Deglución atípica
                        </label>
                    </div>
   
                    <div class="form-group">
                        <input type="checkbox" name="bruxismo" id="bruxismo" autocomplete="off" />
                            <label for="bruxismo" class="btn btn-default active">
                            Bruxismo
                            </label>
                    </div>
   
                    <div class="form-group">
                        <input type="checkbox" name="musculo" id="musculo" autocomplete="off" />
                            <label for="musculo" class="btn btn-default active">
                            Músculo
                            </label>
                    </div>
           
                    <div class="form-group">
                        <input type="checkbox" name="atm" id="atm" autocomplete="off" />
                            <label for="atm" class="btn btn-default active">
                            ATM
                            </label>
                    </div> 

                </div>

                <div class="col-sm-2"></div>    

            </div>


            <div class="form-group row">

              <label for="observacionesfuncional" class="col-sm-2 col-form-label">Observaciones</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="observacionesfuncional" placeholder="">
              </div>
            </div>


            <div class="form-group row">

                <label class="col-sm-12 col-form-label center">PERIODONTAL</label>
            
            </div>


            <div class="form-group row">

                <div class="col-sm-2"></div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <input type="checkbox" name="inflamadas" id="inflamadas" autocomplete="off" />
                        <label for="inflamadas" class="btn btn-default active">
                        Encías inflamadas
                        </label>
                    </div>
   
                    <div class="form-group">
                        <input type="checkbox" name="placa" id="placa" autocomplete="off" />
                            <label for="placa" class="btn btn-default active">
                            Placa
                            </label>
                    </div>
   
                    <div class="form-group">
                        <input type="checkbox" name="calculo" id="calculo" autocomplete="off" />
                            <label for="calculo" class="btn btn-default active">
                            Cálculo
                            </label>
                    </div>
           
                    <div class="form-group">
                        <input type="checkbox" name="gingival" id="gingival" autocomplete="off" />
                            <label for="gingival" class="btn btn-default active">
                            Recesión Gingival
                            </label>
                    </div> 

                </div>

                <div class="col-sm-2"></div>

            </div>


            <div class="form-group row">

              <label for="insercion" class="col-sm-2 col-form-label">Pérdida de la inserción general</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="insercion" placeholder="">
              </div>
            </div>


            <div class="form-group row">

              <label for="movilidad" class="col-sm-2 col-form-label">Movilidad</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="movilidad" placeholder="">
              </div>
            </div>


            <div class="form-group row">

              <label for="observacionesperi" class="col-sm-2 col-form-label">Observaciones</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="observacionesperi" placeholder="">
              </div>
            </div>


            <div class="form-group row">

                <label class="col-sm-12 col-form-label center">PRUEBA DE SENSIBILIDAD A LA PULPA</label>
            
            </div>


            <div class="form-group row">

              <label for="frio" class="col-sm-2 col-form-label">Frío</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="frio" placeholder="">
              </div>
            </div>


            <div class="form-group row">

              <label for="caliente" class="col-sm-2 col-form-label">Caliente</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="caliente" placeholder="">
              </div>
            </div>

            <div class="form-group row">

              <label for="electrica" class="col-sm-2 col-form-label">Prueba eléctrica</label>
              <div class="col-sm-10">
                <div class="form-group">
                    <input type="checkbox" name="calculo" id="calculo" autocomplete="off" />
                </div>
              </div>
            </div>


            <div class="form-group row">

                <label class="col-sm-12 col-form-label center">PRUEBA DE SENSIBILIDAD PERIODONTAL</label>
            
            </div>


            <div class="form-group row">

              <label for="horizontal" class="col-sm-2 col-form-label">Percusión horizontal</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="horizontal" placeholder="">
              </div>
            </div>


            <div class="form-group row">

              <label for="vertical" class="col-sm-2 col-form-label">Percusión vertical</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="vertical" placeholder="">
              </div>
            </div>

            <div class="form-group row">

              <label for="observacionessensibilidad" class="col-sm-2 col-form-label">Observaciones</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="observacionessensibilidad" placeholder="">
              </div>
            </div>


            <div class="form-group row">

                <div class="col-sm-12 col-form-label center">

                <button type="button" class="btn btn-success">Guardar</button>

                </div>
            
            </div>



          </form>
          ';
        }


		// Presend form
		$modelmail='thirdparty';
		$defaulttopic='Information';
		$diroutput = $conf->societe->dir_output;
		$trackid = 'thi'.$object->id;

		include DOL_DOCUMENT_ROOT.'/core/tpl/card_presend.tpl.php';
    }
}

// End of page
llxFooter();
$db->close();
